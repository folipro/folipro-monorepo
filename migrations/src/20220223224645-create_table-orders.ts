import { Knex } from 'knex';

const TABLE_NAME = 'orders';

export async function up(knex: Knex): Promise<void> {
  return await knex.schema.createTable(TABLE_NAME, table => {
    table.bigIncrements('id').primary();

    table.bigInteger('clientId').unsigned().index().references('id').inTable('clients').notNullable();

    table.string('paymentTransactionDate').notNullable();
    table.float('totalAmountWithoutShippingCost').notNullable();
    table.float('totalAmount').notNullable();
    table.string('products').notNullable();

    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').nullable();
    table.timestamp('deletedAt').nullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return await knex.schema.dropTable(TABLE_NAME);
}