import { Knex } from 'knex';

const TABLE_NAME = 'categories';

export async function up(knex: Knex): Promise<void> {
  return await knex.schema.createTable(TABLE_NAME, table => {
    table.bigIncrements('id').primary();

    table.string('name').notNullable();
    table.string('color').notNullable();
    table.integer('daysToRememberValue').notNullable();

    table.unique(['name', 'color', 'daysToRememberValue']);

    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').nullable();
    table.timestamp('deletedAt').nullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return await knex.schema.dropTable(TABLE_NAME);
}