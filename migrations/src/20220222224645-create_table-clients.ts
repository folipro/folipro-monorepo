import { Knex } from 'knex';

const TABLE_NAME = 'clients';

export async function up(knex: Knex): Promise<void> {
  return await knex.schema.createTable(TABLE_NAME, table => {
    table.bigIncrements('id').primary();

    table.string('email').notNullable();

    table.string('name').nullable();
    table.string('nameSearchable').nullable();
    table.string('cpfOrCnpj').nullable();
    table.string('birthday').nullable();
    table.string('phoneNumber').nullable();
    table.string('address').nullable();

    table.unique(['email']);

    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').nullable();
    table.timestamp('deletedAt').nullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return await knex.schema.dropTable(TABLE_NAME);
}