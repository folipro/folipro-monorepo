import { Knex } from 'knex';

const TABLE_NAME = 'tasks';

export async function up(knex: Knex): Promise<void> {
  return await knex.schema.createTable(TABLE_NAME, table => {
    table.bigIncrements('id').primary();

    table.bigInteger('clientId').unsigned().index().references('id').inTable('clients').notNullable();
    table.bigInteger('orderId').unsigned().index().references('id').inTable('orders').notNullable();
    table.bigInteger('categoryId').unsigned().index().references('id').inTable('categories').nullable();

    table.boolean('isFinished').defaultTo(false).notNullable();
    table.string('observations').notNullable();

    table.string('dayToBeRemembered').nullable();

    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').nullable();
    table.timestamp('deletedAt').nullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return await knex.schema.dropTable(TABLE_NAME);
}