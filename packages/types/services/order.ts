import { DatesAndId, Remove } from '../shared';

export namespace OrderModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;
  export type WithoutDates = WithoutDatesAndId & Pick<DatesAndId, 'id'>;

  export type Complete = DatesAndId & {
    clientId: number;
    totalAmount: number;
    totalAmountWithoutShippingCost: number;
    paymentTransactionDate: string;
    products: OrderHelpers.OrderProduct[];
  }
}

export namespace OrderHelpers {
  export type OrderProduct = {
    quantity: number;
    amount: number;
    title: string;
  }
}

export namespace YampiWebhook {
  export type RequestBody = {
    event: 'order.status.updated';
    time: string;
    merchant: { alias: 'folipro'; id: 32848; }
    resource: {
      authorized: boolean;
      b2w_label_url?: string | null;
      billet_whatsapp_link?: string | null; // Pode ser util
      cart_token: string;
      content_statement_url: string; // URL da declaracao de conteudo (pode ser util)
      created_at: {
        date: string;
        timezone: string;
        timezone_type: number;
      }
      customer: {
        data: {
          active: boolean;
          marketplace_id?: string | null;
          merchant_id: number; // 32848;
          birthday?: string | null;
          cpf?: string | null;
          cnpj?: string | null;
          email: string;
          first_name: string;
          last_name: string;
          generic_name: string;
          name: string; // Nome completo
          id: number // RG
          ip: string;
          newsletter: boolean;
          notes: null | any; // VER O QUE É ISSO;
          razao_social: string | null;
          social_driver: string | null;
          social_id: string | null;
          state_registration: string | null;
          token: string;
          type: string;
          created_at: {
            date: string;
            timezone: string;
            timezone_type: number;
          }
          updated_at: {
            date: string;
            timezone: string;
            timezone_type: number;
          }
          phone: {
            area_code: string;
            formated_number: string;
            full_number: string;
            number: string;
            whatsapp_link: string;
          }
        }
      }
      customer_id: number;
      date_delivery: {
        date: string;
        timezone: string;
        timezone_type: number;
      }
      days_delivery: number;
      delivered: boolean;
      desire_status: Array<'delivered' | 'cancelled' | 'shipment_exception'>;
      desire_status_id: Array<7 | 8 | 11>;
      device: 'desktop';
      has_recomm: boolean;
      has_upsell: boolean;
      id: number;
      ip: string;
      is_upsell: boolean;
      items: {
        data: Array<{
          bundle_id?: string | null;
          bundle_name?: string | null;
          customizations: any; // VER ISSO;
          gift: boolean;
          gift_value: number;
          has_recomm: 0 | 1;
          id: number;
          is_digital: boolean;
          item_sku: string;
          price: number;
          price_cost: number;
          product_id: number;
          quantity: number;
          shipment_cost: number;
          sku_id: number;
          sku: {
            data: {
              allow_sell_without_customization: boolean;
              availability: number;
              availability_soldout: number;
              barcode?: string | null;
              blocked_sale: boolean;
              combinations: string;
              days_availability_formated: string;
              erp_id?: string | null;
              id: number;
              image_reference_sku_id?: string | null;
              order: number;
              price_cost: number;
              price_discount: number;
              price_sale: number;
              product_id: number;
              purchase_url: string;
              quantity_managed: boolean;
              seller_id?: number | null;
              sku_id: number;
              sku: string;
              title: string;
              total_in_stock: number;
              total_orders?: number | null;
              height: number;
              length: number;
              weight: number;
              width: number;
              customizations: {
                data: any //VER ISSO;
              }
              created_at: {
                date: string;
                timezone: string;
                timezone_type: number;
              }
              updated_at: {
                date: string;
                timezone: string;
                timezone_type: number;
              }
            }
          }
        }>;
      }
      marketplace: {
        data: any; // VER ISSO;
      }
      marketplace_account: {
        data: any; // VER ISSO;
      }
      marketplace_account_id?: string | null;
      marketplace_id?: string | null;
      marketplace_partner_id?: string | null;
      marketplace_sale_number?: number | null;
      merchant_id: number;
      metadata: {
        data: Array<{ key: string; value: string; }>;
      }
      number: number;
      payments: Array<{
        alias: string;
        icon_url: string;
        name: string;
      }>;
      promocode: {
        data: any; // VER ISSO;
      }
      promocode_id?: string | null;
      public_url: string;
      reorder_url: string;
      shipping_address: {
        data: {
          address_name?: string | null;
          city: string;
          complement: string;
          country: string;
          full_address: string;
          id: number;
          neighborhood: string;
          number: string;
          order_id: number;
          receiver: string;
          street: string;
          uf: string;
          state: string;
          zip_code: string;
          zipcode: string;
        }
      }
      spreadsheet: {
        data: Array<{
          credit_card: string;
          customer: string;
          customer_document: string;
          customer_email: string;
          customer_phone: string;
          customization: string;
          delivered: 0 | 1;
          gateway_transaction_id: string;
          payment: string;
          payment_date: string;
          product: string;
          quantity: number;
          shipping_address: string;
          shipping_city: string;
          shipping_complement: string;
          shipping_neighborhood: string;
          shipping_number: string;
          shipping_state: string;
          shipping_street: string;
          shipping_zip_code: string;
          sku: string;
          status: string;
          total_cost: number;
          total_item: number;
        }>;
      }
      status: {
        data: {
          alias: string;
          description: string;
          id: number;
          name: string;
        }
      }
      status_id: number;
      statuses: {
        data: Array<{
          alias: string;
          description: string;
          id: number;
          name: string;
        }>;
      }
      sync_by_erp: boolean;
      total_comments: number;
      track_code: string;
      track_url: string;
      tansactions: {
        data: Array<{
          affiliation_id: number;
          amount: number;
          antifraud_sale_id?: string | null;
          antifraud_score?: string | null;
          antifraud_status?: string | null;
          authorized: boolean;
          installment_formated: string;
          installment_value: number;
          installments: number;
        }>;
      }
      value_discount: number;
      value_products: number;
      value_shipment: number;
      value_tax: number;
      value_total: number;
      transactions: {
        data: Array<{
          bank_name?: string;
          gateway_order_id?: string;
          captured_at: {
            timezone: string;
            timezone_type: number;
            date: string;
          },
          customer_id: number;
          payment: {
            data: {
              name: string;
              alias: string;
              is_credit_card: boolean,
              active_config: boolean,
              is_billet: boolean,
              has_config: boolean,
              is_deposit: boolean,
              id: number;
              icon_url: string;
              is_pix: boolean
            }
          },
          billet_barcode?: string;
          cancelled_at?: string;
          id: number;
          metadata: { data: [] },
          truncated_card: string;
          captured: boolean,
          payment_id: number;
          installment_value: number;
          billet_our_number?: string;
          marketplace_account_id?: string;
          gateway_authorization_code?: string;
          installment_formated: string;
          status: string;
          antifraud_sale_id?: string;
          holder_document: string;
          affiliation_id: number;
          total_logs: number;
          error_code?: string;
          capture_date?: string;
          gateway_billet_id?: string;
          marketplace_id?: string;
          holder_name: string;
          billet_whatsapp_link?: string;
          created_at: {
            date: string;
            timezone: string;
            timezone_type: number;
          },
          antifraud_status?: string;
          billet_document_number?: string;
          sent_to_antifraud: boolean,
          authorized: boolean,
          billet_date?: string;
          antifraud_score?: string;
          authorized_at: {
            date: string;
            timezone_type: number;
            timezone: string;
          },
          installments: number;
          cancelled: boolean,
          bank_alias?: string;
          error_message?: string;
          amount: number;
          updated_at: {
            timezone: string;
            date: string;
            timezone_type: number;
          },
          billet_url?: string;
          gateway_transaction_id: string;
        }>;
      }
    }
  }
}