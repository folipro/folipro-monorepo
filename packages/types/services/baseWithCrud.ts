import { DatesAndId, Remove } from '../shared';

export namespace BaseWithCrudModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;
  export type WithoutDates = WithoutDatesAndId & Pick<DatesAndId, 'id'>;

  export type Complete = DatesAndId & {
    data: any;
    foo: 'bar';
  }
}