import { DatesAndId, Remove } from '../shared';

export namespace CategoryModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;
  export type WithoutDates = WithoutDatesAndId & Pick<DatesAndId, 'id'>;

  export type Complete = DatesAndId & {
    name: string;
    color: string;
    daysToRememberValue: number;
  }
}