import { DatesAndId, Remove } from '../shared';

export namespace ClientModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;
  export type WithoutDates = WithoutDatesAndId & Pick<DatesAndId, 'id'>;

  export type Complete = DatesAndId & {
    email: string;
    name?: string;
    nameSearchable?: string;
    cpfOrCnpj?: string;
    birthday?: string;
    phoneNumber: ClientHelpers.PhoneNumber;
    address: ClientHelpers.Address;
  }
}
export namespace ClientHelpers {
  export type PhoneNumber = {
    complete: string;
    formatted: string;
    onlyDigits: string;
    dddCode: string;
  }

  export type Address = {
    street: string;
    state: string;
    city: string;
    neighborhood: string;
    complement?: string;
    number?: string;
  }
}