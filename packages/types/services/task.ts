import { DatesAndId, Remove } from '../shared';

export namespace TaskModel {
  export type WithoutDatesAndId = Remove<Complete, keyof DatesAndId>;
  export type WithoutDates = WithoutDatesAndId & Pick<DatesAndId, 'id'>;

  export type Complete = DatesAndId & {
    orderId: number;
    clientId: number;
    categoryId: number | null;
    dayToBeRemembered?: string;
    isFinished: boolean;
    observations: string[];
  }
}