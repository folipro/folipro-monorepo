type ServiceMap = {
  [ServName in ServicesNames]: {
    port: number;
  }
}

export enum ServicesNames {
  Base = 'Base',
  BaseWithCrud = 'BaseWithCrud',
  Api = 'api',
  Category = 'category',
  Client = 'client',
  Order = 'order',
  Task = 'task',
}

export const Service: ServiceMap = {
  [ServicesNames.Base]: { port: 4998 },
  [ServicesNames.BaseWithCrud]: { port: 4999 },
  [ServicesNames.Api]: { port: 5000 },
  [ServicesNames.Category]: { port: 5001 },
  [ServicesNames.Client]: { port: 5002 },
  [ServicesNames.Order]: { port: 5003 },
  [ServicesNames.Task]: { port: 5004 },
}

export type DatesAndId = {
  id: number;
  createdAt: Date;
  updatedAt?: Date | null;
  deletedAt?: Date | null;
}

export type Remove<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export type OrderBy = {
  orderBy?: 'createdAt' | 'id';
  order?: 'asc' | 'desc';
}

export type PaginatedParams = {
  limit: number,
  page: number
}

export type PaginatedReturn = {
  total: number,
  limit: number,
  page: number
}