import 'dotenv/config';
import { Request, Response, NextFunction } from 'express';
import { ForbiddenError } from '@folipro/error';

const { INTERNAL_API_KEY } = process.env;
if (!INTERNAL_API_KEY) {
  throw new Error('process.env.INTERNAL_API_KEY is missing');
}

export const verifyInternalApiKey = (request: Request, response: Response, next: NextFunction) => {
  const isInternal = request.originalUrl.includes('/internal');
  
  if (!isInternal) {
    return next();
  }

  const internalApiKeyFromRequest = request.headers['internal-api-key'];

  if (!internalApiKeyFromRequest) {
    throw new ForbiddenError('Internal Api Key is missing.');
  }

  if (internalApiKeyFromRequest !== INTERNAL_API_KEY) {
    throw new ForbiddenError('Invalid Internal Api Key.');
  }

  return next();
}