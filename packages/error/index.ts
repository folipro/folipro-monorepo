export enum AppErrors {
  BadRequest = 'BadRequestError',
  Unauthorized = 'UnauthorizedError',
  Forbidden = 'ForbiddenError',
  InvalidParam = 'InvalidParamError',
  Communication = 'CommunicationError'
}

export class BadRequestError extends Error {
  public statusCode: number;
  public isAppError: boolean;

  constructor(message: string) {
    super(message);
    this.name = AppErrors.BadRequest;
    this.stack = message;
    this.statusCode = 400;
    this.isAppError = true;
  }
}

export class UnauthorizedError extends Error {
  public statusCode: number;
  public isAppError: boolean;

  constructor(message: string) {
    super(message);
    this.name = AppErrors.Unauthorized;
    this.stack = message;
    this.statusCode = 401;
    this.isAppError = true;
  }
}

export class ForbiddenError extends Error {
  public statusCode: number;
  public isAppError: boolean;

  constructor(message: string) {
    super(message);
    this.name = AppErrors.Forbidden;
    this.stack = message;
    this.statusCode = 403;
    this.isAppError = true;
  }
}

export class InvalidParamError {
  constructor(
    public message: string | string[],
    public statusCode = 422,
    public isAppError: boolean = true,
    public name: string = AppErrors.InvalidParam
  ) {}
}

export class CommunicationError {
  constructor(
    public message: string[],
    public statusCode: number,
    public isAppError: boolean = true,
    public name: string = AppErrors.Communication,
  ) {}
}