import { Joi } from '@folipro/validator';
import { Request, Response, Router as MakeRouter } from 'express';
import { InvalidParamError } from '@folipro/error';
import { DefaultServiceRouter } from './types';

export const Router = MakeRouter;

export const routerHandler = <T extends DefaultServiceRouter>(routesData: T) => {
  return async (request: Request, response: Response) => {
    try {
      const errors = validateSchema(request.body, Joi.object({
        method: Joi.string().required(),
        params: Joi.object()
      }));
      if (errors.length) throw new InvalidParamError(errors);

      const currentHandler = routesData[request.body.method];
      if (!currentHandler) throw new InvalidParamError(`"method" with name "${request.body.method}" not exists.`);

      const { handler, schema, schemaOptions } = currentHandler;

      const errorsArray = validateSchema(request.body.params || {}, schema, schemaOptions);
      if (errorsArray.length) throw new InvalidParamError(errorsArray);

      const result = await handler(request.body.params);
      return response.json(result);
    } catch (err: any) {
      if (err.name && err.isAppError) {
        let errors;
        if (typeof err.message === 'string') errors = [err.message];
        else errors = err.message;

        return response.status(Number(err.statusCode)).json({ errors });
      } else {
        console.error(`
          -----
          [${new Date().toLocaleString()}] OPS! ROUTE HANDLER ERROR \n
          Error details: ${JSON.stringify(err)} \n
          Request BODY: ${JSON.stringify(request.body)} \n
          Request PARAMS: ${JSON.stringify(request.params)} \n
          Request URL: ${JSON.stringify(request.url)} \n
          Request BASE URL: ${JSON.stringify(request.baseUrl)} \n
          Request ORIGINAL URL: ${JSON.stringify(request.originalUrl)} \n
          -----
        `);
        throw err;
      }
    }
  }
}

const validateSchema = (requestBody: object, schema: Joi.ObjectSchema, schemaOptions?: Joi.ValidationOptions): string[] => {
  const errorsArray = [];

  const { error } = schema.validate(requestBody, { abortEarly: false, ...schemaOptions });
  if (error) {
    for (const err of error.details) {
      errorsArray.push(err.message);
    }
  }

  return errorsArray;
}