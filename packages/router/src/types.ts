import { ValidationOptions, ObjectSchema } from '@folipro/validator';

export type DefaultServiceRouter = {
  [methodName: string]: ServiceRouterHandler<any, any>;
}

export type ServiceRouterHandler<Input, Output> = {
  handler: (input: Input) => Promise<Output>;
  schema: ObjectSchema<Input>;
  schemaOptions?: ValidationOptions;
}

export type Url = '/public' | '/internal' | '/public/ping' | '/api';