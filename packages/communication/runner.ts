import 'dotenv/config';
import axios from 'axios';
import { Service, ServicesNames } from '@folipro/types/shared';
import { CommunicationError } from '@folipro/error';

const { INTERNAL_API_KEY, APP_MODE, GATEWAY_BASE_URL } = process.env;
if (!INTERNAL_API_KEY) {
  throw new Error('Not found INTERNAL_API_KEY, APP_MODE or GATEWAY_BASE_URL in process.env');
}

export const createCommunicationRunner = (serviceName: ServicesNames) => {
  const baseURL = _getBaseUrl(serviceName);

  const api = axios.create({
    baseURL,
    headers: { 'Internal-Api-Key': INTERNAL_API_KEY }
  });

  
  return (method: string, params?: any) => {
    const body = { method, params }

    console.log(`
      Beggining to make request to:\n
      URL ${baseURL}/internal\n
      Body: ${JSON.stringify(body)}
    `);

    return api.post('/internal', body)
      .then(res => res.data)
      .catch(err => _errorHandler(err, serviceName, method, params));
  }
}

const _errorHandler = (err: any, serviceName: string, method: string, params?: any): any => {
  if (err?.response?.data) {
    const { status, data } = err.response;
    console.warn(`
      -----
      [${new Date().toLocaleString()}] OPS! ERROR... at least we've already handled! \n
      Service: ${serviceName} \n
      Method: ${method} \n
      Sent params: ${JSON.stringify(params) || 'No body sent'} \n
      Error details: ${JSON.stringify(err)} \n
      -----
    `);
    throw new CommunicationError(data.errors, status);
  }

  console.error(`
    -----
    [${new Date().toLocaleString()}] UNKNOWN ERROR \n
    Service: ${serviceName} \n
    Method: ${method} \n
    Sent params: ${JSON.stringify(params) || 'No body sent'} \n
    Error details: ${JSON.stringify(err)} \n
    -----
  `);
  throw err;
}

const _getBaseUrl = (serviceName: ServicesNames): string => {
  if (APP_MODE === 'local') {
    const servicePort = Service[serviceName].port;
    return `http://localhost:${servicePort}/${serviceName}`;
  } else {
    return `${GATEWAY_BASE_URL}/${serviceName}`;
  }
}