import { ServicesNames } from '@folipro/types/shared';
import { createCommunicationRunner } from '../runner';
import {
  CategoryInput as Input,
  CategoryOutput as Output,
  CategoryMethods as Methods
} from '../types/category';

const run = createCommunicationRunner(ServicesNames.Category);

type CategoryService = {
  pingInternal: (input: Input.PingInternal) => Promise<Output.PingInternal>;
  create: (input: Input.Create) => Promise<Output.Create>;
  update: (input: Input.Update) => Promise<Output.Update>;
  delete: (input: Input.Delete) => Promise<Output.Delete>;
  getOne: (input: Input.GetOne) => Promise<Output.GetOne>;
  getMany: (input: Input.GetMany) => Promise<Output.GetMany>;
  getManyPaginated: (input: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;
};

export const categoryService: CategoryService = {
  pingInternal: data => run(Methods.PingInternal, data),
  create: data => run(Methods.Create, data),
  update: data => run(Methods.Update, data),
  delete: data => run(Methods.Delete, data),
  getOne: data => run(Methods.GetOne, data),
  getMany: data => run(Methods.GetMany, data),
  getManyPaginated: data => run(Methods.GetManyPaginated, data),
};