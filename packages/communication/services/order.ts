import { ServicesNames } from '@folipro/types/shared';
import { createCommunicationRunner } from '../runner';
import {
  OrderInput as Input,
  OrderOutput as Output,
  OrderMethods as Methods
} from '../types/order';

const run = createCommunicationRunner(ServicesNames.Order);

type OrderService = {
  pingInternal: (input: Input.PingInternal) => Promise<Output.PingInternal>;
  create: (input: Input.Create) => Promise<Output.Create>;
  update: (input: Input.Update) => Promise<Output.Update>;
  delete: (input: Input.Delete) => Promise<Output.Delete>;
  getOne: (input: Input.GetOne) => Promise<Output.GetOne>;
  getMany: (input: Input.GetMany) => Promise<Output.GetMany>;
  getManyPaginated: (input: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;
};

export const orderService: OrderService = {
  pingInternal: data => run(Methods.PingInternal, data),
  create: data => run(Methods.Create, data),
  update: data => run(Methods.Update, data),
  delete: data => run(Methods.Delete, data),
  getOne: data => run(Methods.GetOne, data),
  getMany: data => run(Methods.GetMany, data),
  getManyPaginated: data => run(Methods.GetManyPaginated, data),
};