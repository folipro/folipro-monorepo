import { ServicesNames } from '@folipro/types/shared';
import { createCommunicationRunner } from '../runner';
import {
  ClientInput as Input,
  ClientOutput as Output,
  ClientMethods as Methods
} from '../types/client';

const run = createCommunicationRunner(ServicesNames.Client);

type ClientService = {
  pingInternal: (input: Input.PingInternal) => Promise<Output.PingInternal>;
  create: (input: Input.Create) => Promise<Output.Create>;
  update: (input: Input.Update) => Promise<Output.Update>;
  delete: (input: Input.Delete) => Promise<Output.Delete>;
  getOne: (input: Input.GetOne) => Promise<Output.GetOne>;
  getMany: (input: Input.GetMany) => Promise<Output.GetMany>;
  getManyLike: (input: Input.GetManyLike) => Promise<Output.GetManyLike>;
  getManyPaginated: (input: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;
};

export const clientService: ClientService = {
  pingInternal: data => run(Methods.PingInternal, data),
  create: data => run(Methods.Create, data),
  update: data => run(Methods.Update, data),
  delete: data => run(Methods.Delete, data),
  getOne: data => run(Methods.GetOne, data),
  getMany: data => run(Methods.GetMany, data),
  getManyLike: data => run(Methods.GetManyLike, data),
  getManyPaginated: data => run(Methods.GetManyPaginated, data),
};