import { ServicesNames } from '@folipro/types/shared';
import { createCommunicationRunner } from '../runner';
import {
  BaseInput as Input,
  BaseOutput as Output,
  BaseMethods as Methods
} from '../types/base';

const run = createCommunicationRunner(ServicesNames.Base);

type BaseService = {
  pingInternal: (input: Input.PingInternal) => Promise<Output.PingInternal>;
};

export const baseService: BaseService = {
  pingInternal: data => run(Methods.PingInternal, data),
};