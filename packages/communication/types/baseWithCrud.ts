import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@folipro/router';
import { BaseWithCrudModel as Model } from '@folipro/types/services/baseWithCrud';
import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';

export enum BaseWithCrudMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  Update = 'Update',
  Delete = 'Delete',
  GetOne = 'GetOne',
  GetMany = 'GetMany',
  GetManyPaginated = 'GetManyPaginated'
}

export namespace BaseWithCrudInput {
  export type PingInternal = void;
  export type Create = Model.WithoutDatesAndId;
  export type Update = Partial<Model.WithoutDatesAndId> & Pick<Model.Complete, 'id'>;
  export type Delete = Pick<Model.Complete, 'id'>;
  export type GetOne = { where: Partial<Model.WithoutDates>; }
  export type GetMany = void | { where: Partial<Model.WithoutDates>; };
  export type GetManyPaginated = void | Partial<PaginatedParams & { where: Partial<Model.WithoutDates>; }>;
}

export namespace BaseWithCrudOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type Update = Model.Complete;
  export type Delete = boolean;
  export type GetOne = Model.Complete | undefined;
  export type GetMany = Model.Complete[];
  export type GetManyPaginated = PaginatedReturn & { data: Model.Complete[]; }
}

export interface BaseWithCrudServiceRouter extends DefaultServiceRouter {
  [BaseWithCrudMethods.PingInternal]: Handler<BaseWithCrudInput.PingInternal, BaseWithCrudOutput.PingInternal>;
  [BaseWithCrudMethods.Create]: Handler<BaseWithCrudInput.Create, BaseWithCrudOutput.Create>;
  [BaseWithCrudMethods.Update]: Handler<BaseWithCrudInput.Update, BaseWithCrudOutput.Update>;
  [BaseWithCrudMethods.Delete]: Handler<BaseWithCrudInput.Delete, BaseWithCrudOutput.Delete>;
  [BaseWithCrudMethods.GetOne]: Handler<BaseWithCrudInput.GetOne, BaseWithCrudOutput.GetOne>;
  [BaseWithCrudMethods.GetMany]: Handler<BaseWithCrudInput.GetMany, BaseWithCrudOutput.GetMany>;
  [BaseWithCrudMethods.GetManyPaginated]: Handler<BaseWithCrudInput.GetManyPaginated, BaseWithCrudOutput.GetManyPaginated>;
}