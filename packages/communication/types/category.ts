import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@folipro/router';
import { CategoryModel as Model } from '@folipro/types/services/category';
import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';

export enum CategoryMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  Update = 'Update',
  Delete = 'Delete',
  GetOne = 'GetOne',
  GetMany = 'GetMany',
  GetManyPaginated = 'GetManyPaginated'
}

export namespace CategoryInput {
  export type PingInternal = void;
  export type Create = Model.WithoutDatesAndId;
  export type Update = Partial<Model.WithoutDatesAndId> & Pick<Model.Complete, 'id'>;
  export type Delete = Pick<Model.Complete, 'id'>;
  export type GetOne = { where: Partial<Model.WithoutDates>; }
  export type GetMany = void | { where: Partial<Model.WithoutDates>; };
  export type GetManyPaginated = void | Partial<PaginatedParams & { where: Partial<Model.WithoutDates>; }>;
}

export namespace CategoryOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type Update = Model.Complete;
  export type Delete = boolean;
  export type GetOne = Model.Complete | undefined;
  export type GetMany = Model.Complete[];
  export type GetManyPaginated = PaginatedReturn & { data: Model.Complete[]; }
}

export interface CategoryServiceRouter extends DefaultServiceRouter {
  [CategoryMethods.PingInternal]: Handler<CategoryInput.PingInternal, CategoryOutput.PingInternal>;
  [CategoryMethods.Create]: Handler<CategoryInput.Create, CategoryOutput.Create>;
  [CategoryMethods.Update]: Handler<CategoryInput.Update, CategoryOutput.Update>;
  [CategoryMethods.Delete]: Handler<CategoryInput.Delete, CategoryOutput.Delete>;
  [CategoryMethods.GetOne]: Handler<CategoryInput.GetOne, CategoryOutput.GetOne>;
  [CategoryMethods.GetMany]: Handler<CategoryInput.GetMany, CategoryOutput.GetMany>;
  [CategoryMethods.GetManyPaginated]: Handler<CategoryInput.GetManyPaginated, CategoryOutput.GetManyPaginated>;
}