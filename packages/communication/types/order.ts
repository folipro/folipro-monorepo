import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@folipro/router';
import { OrderModel as Model } from '@folipro/types/services/order';
import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';

export enum OrderMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  Update = 'Update',
  Delete = 'Delete',
  GetOne = 'GetOne',
  GetMany = 'GetMany',
  GetManyPaginated = 'GetManyPaginated'
}

export namespace OrderInput {
  export type PingInternal = void;
  export type Create = Model.WithoutDatesAndId;
  export type Update = Partial<Model.WithoutDatesAndId> & Pick<Model.Complete, 'id'>;
  export type Delete = Pick<Model.Complete, 'id'>;
  export type GetOne = { where: Partial<Model.WithoutDates>; }
  export type GetMany = void | { where: Partial<Model.WithoutDates>; };
  export type GetManyPaginated = void | Partial<PaginatedParams & { where: Partial<Model.WithoutDates>; }>;
}

export namespace OrderOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type Update = Model.Complete;
  export type Delete = boolean;
  export type GetOne = Model.Complete | undefined;
  export type GetMany = Model.Complete[];
  export type GetManyPaginated = PaginatedReturn & { data: Model.Complete[]; }
}

export interface OrderServiceRouter extends DefaultServiceRouter {
  [OrderMethods.PingInternal]: Handler<OrderInput.PingInternal, OrderOutput.PingInternal>;
  [OrderMethods.Create]: Handler<OrderInput.Create, OrderOutput.Create>;
  [OrderMethods.Update]: Handler<OrderInput.Update, OrderOutput.Update>;
  [OrderMethods.Delete]: Handler<OrderInput.Delete, OrderOutput.Delete>;
  [OrderMethods.GetOne]: Handler<OrderInput.GetOne, OrderOutput.GetOne>;
  [OrderMethods.GetMany]: Handler<OrderInput.GetMany, OrderOutput.GetMany>;
  [OrderMethods.GetManyPaginated]: Handler<OrderInput.GetManyPaginated, OrderOutput.GetManyPaginated>;
}