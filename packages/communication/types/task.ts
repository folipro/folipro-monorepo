import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@folipro/router';
import { TaskModel as Model } from '@folipro/types/services/task';
import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';

export enum TaskMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  Update = 'Update',
  Delete = 'Delete',
  GetOne = 'GetOne',
  GetMany = 'GetMany',
  GetManyPaginated = 'GetManyPaginated',
}

export namespace TaskInput {
  export type PingInternal = void;
  export type Create = Model.WithoutDatesAndId;
  export type Update = Partial<Model.WithoutDatesAndId> & Pick<Model.Complete, 'id'>;
  export type Delete = Pick<Model.Complete, 'id'>;
  export type GetOne = { where: Partial<Model.WithoutDates>; }
  export type GetMany = void | { where: Partial<Model.WithoutDates>; };
  export type GetManyPaginated = void | Partial<PaginatedParams & { where: Partial<Model.WithoutDates>; }>;
}

export namespace TaskOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type Update = Model.Complete;
  export type Delete = boolean;
  export type GetOne = Model.Complete | undefined;
  export type GetMany = Model.Complete[];
  export type GetManyPaginated = PaginatedReturn & { data: Model.Complete[]; }
}

export interface TaskServiceRouter extends DefaultServiceRouter {
  [TaskMethods.PingInternal]: Handler<TaskInput.PingInternal, TaskOutput.PingInternal>;
  [TaskMethods.Create]: Handler<TaskInput.Create, TaskOutput.Create>;
  [TaskMethods.Update]: Handler<TaskInput.Update, TaskOutput.Update>;
  [TaskMethods.Delete]: Handler<TaskInput.Delete, TaskOutput.Delete>;
  [TaskMethods.GetOne]: Handler<TaskInput.GetOne, TaskOutput.GetOne>;
  [TaskMethods.GetMany]: Handler<TaskInput.GetMany, TaskOutput.GetMany>;
  [TaskMethods.GetManyPaginated]: Handler<TaskInput.GetManyPaginated, TaskOutput.GetManyPaginated>;
}