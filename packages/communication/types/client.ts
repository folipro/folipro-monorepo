import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@folipro/router';
import { ClientModel as Model } from '@folipro/types/services/client';
import { PaginatedParams, PaginatedReturn, Remove } from '@folipro/types/shared';

export enum ClientMethods {
  PingInternal = 'PingInternal',
  Create = 'Create',
  Update = 'Update',
  Delete = 'Delete',
  GetOne = 'GetOne',
  GetMany = 'GetMany',
  GetManyLike = 'GetManyLike',
  GetManyPaginated = 'GetManyPaginated'
}

export namespace ClientInput {
  export type PingInternal = void;
  export type Create = Remove<Model.WithoutDatesAndId, 'nameSearchable'>;
  export type Update = Partial<Create> & Pick<Model.Complete, 'id'>;
  export type Delete = Pick<Model.Complete, 'id'>;
  export type GetOne = { where: Partial<Model.WithoutDates>; }
  export type GetMany = void | { where: Partial<Model.WithoutDates>; };
  export type GetManyLike = void | { whereLike: Partial<Pick<Model.WithoutDates, 'email' | 'nameSearchable' | 'cpfOrCnpj'>>; }
  export type GetManyPaginated = void | Partial<PaginatedParams & { where: Partial<Model.WithoutDates>; }>;
}

export namespace ClientOutput {
  export type PingInternal = string;
  export type Create = Model.Complete;
  export type Update = Model.Complete;
  export type Delete = boolean;
  export type GetOne = Model.Complete | undefined;
  export type GetMany = Model.Complete[];
  export type GetManyLike = Model.Complete[];
  export type GetManyPaginated = PaginatedReturn & { data: Model.Complete[]; }
}

export interface ClientServiceRouter extends DefaultServiceRouter {
  [ClientMethods.PingInternal]: Handler<ClientInput.PingInternal, ClientOutput.PingInternal>;
  [ClientMethods.Create]: Handler<ClientInput.Create, ClientOutput.Create>;
  [ClientMethods.Update]: Handler<ClientInput.Update, ClientOutput.Update>;
  [ClientMethods.Delete]: Handler<ClientInput.Delete, ClientOutput.Delete>;
  [ClientMethods.GetOne]: Handler<ClientInput.GetOne, ClientOutput.GetOne>;
  [ClientMethods.GetMany]: Handler<ClientInput.GetMany, ClientOutput.GetMany>;
  [ClientMethods.GetManyLike]: Handler<ClientInput.GetManyLike, ClientOutput.GetManyLike>;
  [ClientMethods.GetManyPaginated]: Handler<ClientInput.GetManyPaginated, ClientOutput.GetManyPaginated>;
}