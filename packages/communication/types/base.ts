import { DefaultServiceRouter, ServiceRouterHandler as Handler } from '@folipro/router';
// import {} from '@folipro/types/services/base';

export enum BaseMethods {
  PingInternal = 'PingInternal'
}

export namespace BaseInput {
  export type PingInternal = void;
}

export namespace BaseOutput {
  export type PingInternal = string;
}

export interface BaseServiceRouter extends DefaultServiceRouter {
  [BaseMethods.PingInternal]: Handler<BaseInput.PingInternal, BaseOutput.PingInternal>;
}