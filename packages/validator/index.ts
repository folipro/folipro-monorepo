import Joi, { ObjectSchema, ValidationOptions } from 'joi';
import { utilsBr, validateBr } from 'js-brasil';
import { InvalidParamError } from '@folipro/error';

export { Joi, ObjectSchema, ValidationOptions }

export const brazilianValidator = {
  phone: (phone: any) => {
    if (phone) {
      const isValid = validateBr.telefone(phone);
      if (!isValid) throw new InvalidParamError(`Invalid phone ${phone}.`);
      else return utilsBr.getAllDigits(phone);
    } else {
      return '';
    }
  },

  cnpj: (cnpj: any) => {
    if (cnpj) {
      const isValid = validateBr.cnpj(cnpj);
      if (!isValid) throw new InvalidParamError(`Invalid CNPJ ${cnpj}.`);
      else return utilsBr.getAllDigits(cnpj);
    } else {
      return '';
    }
  },

  cpf: (cpf: any) => {
    if (cpf) {
      const isValid = validateBr.cpf(cpf);
      if (!isValid) throw new InvalidParamError(`Invalid CPF ${cpf}.`);
      else return utilsBr.getAllDigits(cpf);
    } else {
      return '';
    }
  },

  cnh: (cnh: any) => {
    if (cnh) {
      const isValid = validateBr.cnh(cnh);
      if (!isValid) throw new InvalidParamError(`Invalid CNH ${cnh}.`);
      else return utilsBr.getAllDigits(cnh);
    } else {
      return '';
    }
  },

  zipCode: (zipCode: any) => {
    if (zipCode) {
      const isValid = validateBr.cep(zipCode);
      if (!isValid) throw new InvalidParamError(`Invalid CEP ${zipCode}.`);
      else return utilsBr.getAllDigits(zipCode);
    } else {
      return '';
    }
  },

  renavam: (renavam: any) => {
    if (renavam) {
      const isValid = validateBr.renavam(renavam);
      if (!isValid) throw new InvalidParamError(`Invalid renavam ${renavam}.`);
      else return utilsBr.getAllDigits(renavam);
    } else {
      return '';
    }
  },

  plate: (plate: any) => {
    if (plate) {
      const isValid = validateBr.placa(plate, true);
      if (!isValid) throw new InvalidParamError(`Invalid plate ${plate}.`);
      else return utilsBr.getAllDigits(plate);
    } else {
      return '';
    }
  },

  chassi: (chassi: any) => {
    if (chassi) {
      const isValid = validateBr.chassi(chassi);
      if (!isValid) throw new InvalidParamError(`Invalid chassi ${chassi}.`);
      else return utilsBr.getAllDigits(chassi);
    } else {
      return '';
    }
  }
}