import { RequestHandler } from 'express';
import { YampiWebhook } from '@folipro/types/services/order';
import { saveYampiSale } from './saveYampiSale';

type Request = RequestHandler<{}, {}, YampiWebhook.RequestBody>;

export const handleYampiWebhook: Request = async (request, response) => {
  try {
    if (request.body.resource.status_id === 7) await saveYampiSale(request.body);
    return response.json({ ok: true });
  } catch (err) {
    console.error(`
      Error [${new Date().toISOString()}] \n
      Error details: ${err}
    `);
    return response.json({ ok: false });
  }
}