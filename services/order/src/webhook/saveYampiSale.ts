import { OrderHelpers, YampiWebhook } from '@folipro/types/services/order';
import { clientService } from '@folipro/communication/services/client';
import { taskService } from '@folipro/communication/services/task';
import * as core from '../core';

export const saveYampiSale = async ({ resource }: YampiWebhook.RequestBody) => {
  const { customer, shipping_address, items, value_shipment, transactions } = resource;

  let clientId: number = 0;

  const clientExists = await clientService.getOne({ where: { email: customer.data.email } });

  if (clientExists) {
    clientId = clientExists.id;
  } else {
    let cpfOrCnpj: string | undefined;
    if (customer.data.cnpj) cpfOrCnpj = customer.data.cnpj;
    if (customer.data.cpf) cpfOrCnpj = customer.data.cpf;

    let name: string | undefined;
    if (customer.data.razao_social) name = customer.data.razao_social;
    if (customer.data.name) name = customer.data.name;

    const createdClient = await clientService.create({
      name,
      cpfOrCnpj,
      email: customer.data.email,
      birthday: customer.data.birthday || undefined,
      address: {
        city: shipping_address.data.city,
        neighborhood: shipping_address.data.neighborhood,
        state: shipping_address.data.state,
        street: shipping_address.data.street,
        complement: shipping_address.data.complement,
        number: shipping_address.data.number
      },
      phoneNumber: {
        complete: customer.data.phone.full_number,
        dddCode: customer.data.phone.area_code,
        formatted: customer.data.phone.formated_number,
        onlyDigits: customer.data.phone.number
      }
    });

    clientId = createdClient.id;
  }

  const finalArray: OrderHelpers.OrderProduct[] = [];
  let totalAmountWithoutShippingCost = 0;

  for (const index in items.data) {
    const order = items.data[index];

    totalAmountWithoutShippingCost += order.price;

    finalArray.push({
      amount: order.price,
      quantity: order.quantity,
      title: order.sku.data.title
    });
  }

  const createdOrder = await core.create({
    clientId,
    totalAmountWithoutShippingCost,
    totalAmount: totalAmountWithoutShippingCost + value_shipment,
    paymentTransactionDate: transactions.data[0].captured_at.date,
    products: finalArray,
  });

  await taskService.create({
    clientId,
    orderId: createdOrder.id,
    isFinished: false,
    observations: []
  });

  console.log('everything is ok!');
}