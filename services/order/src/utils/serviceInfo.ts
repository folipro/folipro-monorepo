import { Service, ServicesNames } from '@folipro/types/shared';

const service = ServicesNames.Order;

export const serviceName = service;
export const servicePort = Service[serviceName].port;