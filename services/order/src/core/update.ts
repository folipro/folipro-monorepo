import { OrderInput as Input, OrderOutput as Output } from '@folipro/communication/types/order';
import { BadRequestError } from '@folipro/error';
import { repository } from '../providers/repository';

type Core = (params: Input.Update) => Promise<Output.Update>;

export const update: Core = async (params) => {
  const updatedData = await repository.update({
    ...params,
    products: params.products && params.products.length ? JSON.stringify(params.products) : undefined
  });

  if (!updatedData) {
    throw new BadRequestError('Data to be updated not found');
  }

  return {
    ...updatedData,
    products: JSON.parse(updatedData.products)
  }
}