import { OrderInput as Input, OrderOutput as Output } from '@folipro/communication/types/order';
import { repository } from '../providers/repository';

type Core = (params: Input.GetOne) => Promise<Output.GetOne>;

export const getOne: Core = async (params) => {
  const foundOrder = await repository.findOne(params);

  if (!foundOrder) return undefined;

  return {
    ...foundOrder,
    products: JSON.parse(foundOrder.products),
  }
}