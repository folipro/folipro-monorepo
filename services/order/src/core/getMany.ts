import { OrderInput as Input, OrderOutput as Output } from '@folipro/communication/types/order';
import { repository } from '../providers/repository';

type Core = (params: Input.GetMany) => Promise<Output.GetMany>;

export const getMany: Core = async (params) => {
  const foundOrders = await repository.findMany(params);
  return foundOrders.map(order => ({
    ...order,
    products: JSON.parse(order.products)
  }));
}