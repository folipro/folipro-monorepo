import { OrderInput as Input, OrderOutput as Output } from '@folipro/communication/types/order';
import { repository } from '../providers/repository';

type Core = (params: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;

export const getManyPaginated: Core = async (params) => {
  const { data, total, limit, page } =  await repository.findManyPaginated(params);

  const mappedData = data.map(order => ({
    ...order,
    products: JSON.parse(order.products),
  }));

  return { data: mappedData, total, limit, page }
}