import { OrderInput as Input, OrderOutput as Output } from '@folipro/communication/types/order';
import { repository } from '../providers/repository';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async (params) => {
  const createdOrder = await repository.insert({
    ...params,
    products: JSON.stringify(params.products)
  });

  return {
    ...createdOrder,
    products: JSON.parse(createdOrder.products)
  }
}