import {
  OrderServiceRouter as ServiceRouter,
  OrderInput as Input,
  OrderMethods as Methods
} from '@folipro/communication/types/order';
import { OrderHelpers, OrderModel as Model } from '@folipro/types/services/order';
import { routerHandler, Router, Url } from '@folipro/router';
import { Joi } from '@folipro/validator';
import { serviceName } from './utils/serviceInfo';
import { handleYampiWebhook } from './webhook';
import * as core from './core';

const url = (url: Url) => `/${serviceName}${url}`;
const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

router.post(url('/public/webhook' as Url), handleYampiWebhook);

const whereJoi = Joi.object<Partial<Model.WithoutDates>>().keys({
  id: Joi.number().optional(),
  clientId: Joi.number().optional(),
  paymentTransactionDate: Joi.string().optional(),
  totalAmount: Joi.number().optional(),
  totalAmountWithoutShippingCost: Joi.number().optional(),
});

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  },

  [Methods.Create]: {
    handler: core.create,
    schema: Joi.object<Input.Create>().keys({
      clientId: Joi.number().required(),
      paymentTransactionDate: Joi.string().required(),
      totalAmount: Joi.number().required(),
      totalAmountWithoutShippingCost: Joi.number().required(),
      products: Joi.array().items(Joi.object<OrderHelpers.OrderProduct>().keys({
        amount: Joi.number().required(),
        quantity: Joi.number().required(),
        title: Joi.string().required(),
      }))
    })
  },

  [Methods.Update]: {
    handler: core.update,
    schema: Joi.object<Input.Update>().keys({
      id: Joi.number().required(),
      clientId: Joi.number().optional(),
      paymentTransactionDate: Joi.string().optional(),
      totalAmount: Joi.number().optional(),
      totalAmountWithoutShippingCost: Joi.number().optional(),
      products: Joi.array().items(Joi.object<OrderHelpers.OrderProduct>().keys({
        amount: Joi.number().optional(),
        quantity: Joi.number().optional(),
        title: Joi.string().optional(),
      }))
    })
  },

  [Methods.GetOne]: {
    handler: core.getOne,
    schema: Joi.object<Input.GetOne>().keys({
      where: whereJoi.required()
    })
  },

  [Methods.GetMany]: {
    handler: core.getMany,
    schema: Joi.object<Input.GetMany>().keys({
      where: whereJoi.optional()
    })
  },

  [Methods.GetManyPaginated]: {
    handler: core.getManyPaginated,
    schema: Joi.object<Input.GetManyPaginated>().keys({
      limit: Joi.number().optional(),
      page: Joi.number().optional(),
      where: whereJoi.optional()
    })
  },

  [Methods.Delete]: {
    handler: core.softDelete,
    schema: Joi.object<Input.Delete>().keys({
      id: Joi.number().required()
    })
  },
}));

export const routes = router;