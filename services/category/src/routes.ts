import {
  CategoryServiceRouter as ServiceRouter,
  CategoryInput as Input,
  CategoryMethods as Methods
} from '@folipro/communication/types/category';
import { CategoryModel as Model } from '@folipro/types/services/category';
import { routerHandler, Router, Url } from '@folipro/router';
import { Joi } from '@folipro/validator';
import { serviceName } from './utils/serviceInfo';
import * as core from './core';

const url = (url: Url) => `/${serviceName}${url}`;
const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

const whereJoi = Joi.object<Partial<Model.WithoutDates>>().keys({
  id: Joi.number().optional(),
  name: Joi.string().optional(),
  color: Joi.string().optional(),
});

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  },

  [Methods.Create]: {
    handler: core.create,
    schema: Joi.object<Input.Create>().keys({
      name: Joi.string().required(),
      color: Joi.string().required(),
      daysToRememberValue: Joi.number().required(),
    })
  },

  [Methods.Update]: {
    handler: core.update,
    schema: Joi.object<Input.Update>().keys({
      id: Joi.number().required(),
      name: Joi.string().optional(),
      color: Joi.string().optional(),
      daysToRememberValue: Joi.number().optional(),
    })
  },

  [Methods.GetOne]: {
    handler: core.getOne,
    schema: Joi.object<Input.GetOne>().keys({
      where: whereJoi.required()
    })
  },

  [Methods.GetMany]: {
    handler: core.getMany,
    schema: Joi.object<Input.GetMany>().keys({
      where: whereJoi.optional()
    })
  },

  [Methods.GetManyPaginated]: {
    handler: core.getManyPaginated,
    schema: Joi.object<Input.GetManyPaginated>().keys({
      limit: Joi.number().optional(),
      page: Joi.number().optional(),
      where: whereJoi.optional()
    })
  },

  [Methods.Delete]: {
    handler: core.softDelete,
    schema: Joi.object<Input.Delete>().keys({
      id: Joi.number().required()
    })
  },
}));

export const routes = router;