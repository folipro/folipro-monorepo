import { CategoryInput as Input, CategoryOutput as Output } from '@folipro/communication/types/category';
import { BadRequestError, InvalidParamError } from '@folipro/error';
import { repository } from '../providers/repository';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async ({ name, color, daysToRememberValue }) => {
  if (color.includes('#') || color.includes('(') || color.includes(')')) {
    throw new InvalidParamError('Color must contain only HEX digits');
  }

  if (color.length > 6) {
    throw new InvalidParamError('Max color HEX digits length is 6');
  }

  const nameAlreadyExists = await repository.findOne({ where: { name } });
  if (nameAlreadyExists) {
    throw new BadRequestError('Category name already exists.');
  }

  const colorAlreadyExists = await repository.findOne({ where: { color } });
  if (colorAlreadyExists) {
    throw new BadRequestError('Category color already exists.');
  }

  const daysToRememberValueAlreadyExists = await repository.findOne({ where: { daysToRememberValue } });
  if (daysToRememberValueAlreadyExists) {
    throw new BadRequestError('Category daysToRememberValue already exists.');
  }

  return await repository.insert({ name, color, daysToRememberValue });
}