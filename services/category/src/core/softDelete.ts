import { CategoryInput as Input, CategoryOutput as Output } from '@folipro/communication/types/category';
import { repository } from '../providers/repository';

type Core = (params: Input.Delete) => Promise<Output.Delete>;

export const softDelete: Core = async (params) => {
  const deletedData = await repository.delete(params);
  if (deletedData) return true;
  else return false;
}