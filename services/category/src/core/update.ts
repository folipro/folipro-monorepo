import { CategoryInput as Input, CategoryOutput as Output } from '@folipro/communication/types/category';
import { BadRequestError, InvalidParamError } from '@folipro/error';
import { repository } from '../providers/repository';

type Core = (params: Input.Update) => Promise<Output.Update>;

export const update: Core = async (params) => {
  if (params.color) {
    if (params.color.includes('#') || params.color.includes('(') || params.color.includes(')')) {
      throw new InvalidParamError('Color must contain only RGBA numbers');
    }
  
    if (params.color.length > 6) {
      throw new InvalidParamError('Max RGBA numbers length is 6');
    }

    const colorAlreadyExists = await repository.findOne({ where: { color: params.color } });
    if (colorAlreadyExists && colorAlreadyExists.id !== params.id) {
      throw new BadRequestError('Category color already exists.');
    }
  }

  if (params.name) {
    const nameAlreadyExists = await repository.findOne({ where: { name: params.name } });
    if (nameAlreadyExists && nameAlreadyExists.id !== params.id) {
      throw new BadRequestError('Category name already exists.');
    }
  }

  if (params.daysToRememberValue) {
    const daysToRememberValueAlreadyExists = await repository.findOne({ where: { daysToRememberValue: params.daysToRememberValue } });
    if (daysToRememberValueAlreadyExists && daysToRememberValueAlreadyExists.id !== params.id) {
      throw new BadRequestError('Category daysToRememberValue already exists.');
    }
  }

  const updatedData = await repository.update(params);
  if (!updatedData) {
    throw new BadRequestError('Data to be updated not found');
  }
  return updatedData;
}