import { CategoryInput as Input, CategoryOutput as Output } from '@folipro/communication/types/category';
import { repository } from '../providers/repository';

type Core = (params: Input.GetMany) => Promise<Output.GetMany>;

export const getMany: Core = async (params) => {
  return await repository.findMany(params);
}