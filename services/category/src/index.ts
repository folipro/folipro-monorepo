import serverless from 'serverless-http';
import { APIGatewayEvent, Context } from 'aws-lambda';
import { serviceName } from './utils/serviceInfo';
import { app } from './app';

export const handle = async (event: APIGatewayEvent, context: Context) => {
  console.log(`
    Received event at service: ${serviceName} \n
    Received event body: ${JSON.stringify(event.body)} \n
    Received event path: ${event.path} \n
    Received event headers: ${JSON.stringify(event.headers)} \n
    Received event httpMethod: ${event.httpMethod} \n
    Received event pathParameters: ${JSON.stringify(event.pathParameters)}
  `);

  try {
    const sls = serverless(app);
    return await sls(event, context);
  } catch (err) {
    console.error(err);
  }
}