import knex from 'knex';
import { ClientModel } from '@folipro/types/services/client';
import { OrderBy, PaginatedParams, Remove } from '@folipro/types/shared';
import { BadRequestError } from '@folipro/error';

type ModelComplete = Remove<ClientModel.Complete, 'phoneNumber' | 'address'> & {
  phoneNumber: string;
  address: string;
}

type ModelWithoutDatesAndId = Remove<ClientModel.WithoutDatesAndId, 'phoneNumber' | 'address'> & {
  phoneNumber: string;
  address: string;
}

const getTable = () => {
  try {
    const database = knex({
      client: 'pg',
      connection: process.env.DB_URL
    });

    return database<ModelComplete>('clients');
  } catch (err) {
    throw new Error(`Error on opening conn with PostgreSQL. Error details: ${err}`);
  }
}

type FindParams = {
  where: Partial<Remove<ModelComplete, 'createdAt' | 'updatedAt' | 'deletedAt' | 'phoneNumber' | 'address'>>;
}

type FindLikeParams = {
  whereLike: Partial<Pick<ModelComplete, 'email' | 'nameSearchable' | 'cpfOrCnpj'>>;
}

export const repository = {
  insert: async (params: ModelWithoutDatesAndId) => {
    try {
      const data = await getTable().insert(params).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  update: async ({ id, ...params }: Partial<ModelWithoutDatesAndId> & Pick<ModelComplete, 'id'>) => {
    try {
      const data = await getTable().where('id', id).whereNull('deletedAt').update({ ...params, updatedAt: new Date() }).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  delete: async ({ id }: Pick<ModelComplete, 'id'>) => {
    try {
      const data = await getTable().where('id', id).whereNull('deletedAt').update({ deletedAt: new Date() }).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findOne: async ({ where }: FindParams) => {
    try {
      let query = getTable();

      if (where) {
        for (const [key, value] of Object.entries(where)) {
          query = query.where(key, value);
        }
      }

      return await query.where('deletedAt', null).first();
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findMany: async (params: Partial<FindParams & OrderBy> | void) => {
    let query = getTable();

    if (params && params.where) {
      for (const [key, value] of Object.entries(params.where)) {
        query = query.where(key, value);
      }
    }

    const orderBy = params && params.orderBy ? params.orderBy : 'createdAt';
    const order = params && params.order ? params.order : 'asc';

    return await query.where('deletedAt', null).orderBy(orderBy, order).select('*');
  },

  findManyPaginated: async (params: void | Partial<FindParams & OrderBy & PaginatedParams>) => {
    let query = getTable();

    const [count] = await query.count('id');

    if (params && params.where) {
      for (const [key, value] of Object.entries(params.where)) {
        query = query.where(key, value);
      }
    }

    const orderBy = params && params.orderBy ? params.orderBy : 'createdAt';
    const order = params && params.order ? params.order : 'asc';
    const limit = params && params.limit ? params.limit : 10;
    const page = params && params.page ? params.page : 1;
    const offset = Math.ceil((page - 1) * limit);

    const data = await query.where('deletedAt', null).orderBy(orderBy, order).limit(limit).offset(offset).select('*');

    return { data, total: +count.CNT, limit, page }
  },

  findManyLike: async (params: void | Partial<FindLikeParams & OrderBy>) => {
    let query = getTable();

    if (params && params.whereLike) {
      for (const [key, value] of Object.entries(params.whereLike)) {
        query = query.whereLike(key, `%${value}%`);
      }
    }

    const orderBy = params && params.orderBy ? params.orderBy : 'createdAt';
    const order = params && params.order ? params.order : 'asc';

    return await query.where('deletedAt', null).orderBy(orderBy, order).select('*');
  },
}