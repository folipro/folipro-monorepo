import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { BadRequestError } from '@folipro/error';
import { makeNameSearchable } from '../utils/makeNameSearchable';
import { normalizeCpfOrCnpj } from '../utils/normalizeCpfOrCnpj';
import { repository } from '../providers/repository';

type Core = (params: Input.Update) => Promise<Output.Update>;

export const update: Core = async (params) => {
  if (params.email) {
    const emailAlreadyExists = await repository.findOne({ where: { email: params.email } });
  
    if (emailAlreadyExists) {
      throw new BadRequestError('Email already exists');
    }
  }

  const nameSearchable = params.name ? makeNameSearchable(params.name) : undefined;

  const cpfOrCnpj = params.cpfOrCnpj ? normalizeCpfOrCnpj(params.cpfOrCnpj) : undefined;

  const updatedData = await repository.update({
    ...params,
    nameSearchable,
    cpfOrCnpj,
    address: params.address ? JSON.stringify(params.address) : undefined,
    phoneNumber: params.phoneNumber ? JSON.stringify(params.phoneNumber) : undefined,
  });

  if (!updatedData) {
    throw new BadRequestError('Data to be updated not found');
  }

  return {
    ...updatedData,
    address: JSON.parse(updatedData.address),
    phoneNumber: JSON.parse(updatedData.phoneNumber)
  }
}