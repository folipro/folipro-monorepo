import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { repository } from '../providers/repository';

type Core = (params: Input.GetMany) => Promise<Output.GetMany>;

export const getMany: Core = async (params) => {
  const foundClients = await repository.findMany(params);

  return foundClients.map(client => ({
    ...client,
    address: JSON.parse(client.address),
    phoneNumber: JSON.parse(client.phoneNumber)
  }));
}