import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { makeNameSearchable } from '../utils/makeNameSearchable';
import { normalizeCpfOrCnpj } from '../utils/normalizeCpfOrCnpj';
import { repository } from '../providers/repository';

type Core = (params: Input.GetManyLike) => Promise<Output.GetManyLike>;

export const getManyLike: Core = async (params) => {
  if (params) {
    if (params.whereLike.cpfOrCnpj) params.whereLike.cpfOrCnpj = normalizeCpfOrCnpj(params.whereLike.cpfOrCnpj);
    if (params.whereLike.nameSearchable) params.whereLike.nameSearchable = makeNameSearchable(params.whereLike.nameSearchable);
  }

  const foundClients = await repository.findManyLike(params);

  return foundClients.map(client => ({
    ...client,
    address: JSON.parse(client.address),
    phoneNumber: JSON.parse(client.phoneNumber)
  }));
}