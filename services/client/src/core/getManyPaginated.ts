import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { repository } from '../providers/repository';

type Core = (params: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;

export const getManyPaginated: Core = async (params) => {
  const { data, total, limit, page } =  await repository.findManyPaginated(params);

  const mappedData = data.map(client => ({
    ...client,
    address: JSON.parse(client.address),
    phoneNumber: JSON.parse(client.phoneNumber)
  }));

  return { data: mappedData, total, limit, page }
}