import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { repository } from '../providers/repository';

type Core = (params: Input.GetOne) => Promise<Output.GetOne>;

export const getOne: Core = async (params) => {
  const foundClient = await repository.findOne(params);
  if (!foundClient) return undefined;
  return {
    ...foundClient,
    address: JSON.parse(foundClient.address),
    phoneNumber: JSON.parse(foundClient.phoneNumber)
  }
}