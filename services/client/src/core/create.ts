import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { BadRequestError } from '@folipro/error';
import { repository } from '../providers/repository';
import { makeNameSearchable } from '../utils/makeNameSearchable';
import { normalizeCpfOrCnpj } from '../utils/normalizeCpfOrCnpj';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async (params) => {
  const emailAlreadyExists = await repository.findOne({ where: { email: params.email } });

  if (emailAlreadyExists) {
    throw new BadRequestError('Email already exists');
  }

  const nameSearchable = params.name ? makeNameSearchable(params.name) : undefined;

  const cpfOrCnpj = params.cpfOrCnpj ? normalizeCpfOrCnpj(params.cpfOrCnpj) : undefined;

  const createdClient = await repository.insert({
    ...params,
    nameSearchable,
    cpfOrCnpj,
    address: JSON.stringify(params.address),
    phoneNumber: JSON.stringify(params.phoneNumber)
  });

  return {
    ...createdClient,
    address: JSON.parse(createdClient.address),
    phoneNumber: JSON.parse(createdClient.phoneNumber)
  }
}