import { ClientInput as Input, ClientOutput as Output } from '@folipro/communication/types/client';
import { repository } from '../providers/repository';

type Core = (params: Input.Delete) => Promise<Output.Delete>;

export const softDelete: Core = async (params) => {
  const deletedData = await repository.delete(params);
  if (deletedData) return true;
  else return false;
}