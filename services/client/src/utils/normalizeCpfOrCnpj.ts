export const normalizeCpfOrCnpj = (cpfOrCnpj: string) => cpfOrCnpj
  .split('-')
  .join('').split('.')
  .join('').split('/')
  .join('')
;