import {
  ClientServiceRouter as ServiceRouter,
  ClientInput as Input,
  ClientMethods as Methods
} from '@folipro/communication/types/client';
import { ClientModel as Model } from '@folipro/types/services/client';
import { routerHandler, Router, Url } from '@folipro/router';
import { Joi } from '@folipro/validator';
import { serviceName } from './utils/serviceInfo';
import * as core from './core';

const url = (url: Url) => `/${serviceName}${url}`;
const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

const whereJoi = Joi.object<Partial<Model.WithoutDates>>().keys({
  id: Joi.number().optional(),
  email: Joi.string().email().optional(),
  name: Joi.string().optional(),
  nameSearchable: Joi.string().optional(),
  cpfOrCnpj: Joi.string().optional(),
  birthday: Joi.string().optional()
});

const whereLikeJoi = Joi.object<Partial<Pick<Model.WithoutDates, 'nameSearchable' | 'email' | 'cpfOrCnpj'>>>().keys({
  email: Joi.string().optional(),
  nameSearchable: Joi.string().optional(),
  cpfOrCnpj: Joi.string().optional(),
});

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  },

  [Methods.Create]: {
    handler: core.create,
    schema: Joi.object<Input.Create>().keys({
      email: Joi.string().email().required(),
      name: Joi.string().optional(),
      cpfOrCnpj: Joi.string().optional(),
      birthday: Joi.string().optional(),
      address: Joi.any().optional(),
      phoneNumber: Joi.any().optional()
    })
  },

  [Methods.Update]: {
    handler: core.update,
    schema: Joi.object<Input.Update>().keys({
      id: Joi.number().required(),
      email: Joi.string().email().optional(),
      name: Joi.string().optional(),
      cpfOrCnpj: Joi.string().optional(),
      birthday: Joi.string().optional(),
      address: Joi.any().optional(),
      phoneNumber: Joi.any().optional()
    })
  },

  [Methods.GetOne]: {
    handler: core.getOne,
    schema: Joi.object<Input.GetOne>().keys({
      where: whereJoi.required()
    })
  },

  [Methods.GetMany]: {
    handler: core.getMany,
    schema: Joi.object<Input.GetMany>().keys({
      where: whereJoi.optional()
    })
  },

  [Methods.GetManyLike]: {
    handler: core.getManyLike,
    schema: Joi.object<Input.GetManyLike>().keys({
      whereLike: whereLikeJoi.optional()
    })
  },

  [Methods.GetManyPaginated]: {
    handler: core.getManyPaginated,
    schema: Joi.object<Input.GetManyPaginated>().keys({
      limit: Joi.number().optional(),
      page: Joi.number().optional(),
      where: whereJoi.optional()
    })
  },

  [Methods.Delete]: {
    handler: core.softDelete,
    schema: Joi.object<Input.Delete>().keys({
      id: Joi.number().required()
    })
  },
}));

export const routes = router;