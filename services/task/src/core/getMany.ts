import { TaskInput as Input, TaskOutput as Output } from '@folipro/communication/types/task';
import { repository } from '../providers/repository';

type Core = (params: Input.GetMany) => Promise<Output.GetMany>;

export const getMany: Core = async (params) => {
  const foundData = await repository.findMany(params);

  return foundData.map(data => ({
    ...data,
    observations: JSON.parse(data.observations)
  }));
}