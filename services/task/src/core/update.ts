import { TaskInput as Input, TaskOutput as Output } from '@folipro/communication/types/task';
import { BadRequestError } from '@folipro/error';
import { repository } from '../providers/repository';

type Core = (params: Input.Update) => Promise<Output.Update>;

export const update: Core = async (params) => {
  const updatedData = await repository.update({
    ...params,
    observations: params.observations && params.observations.length ? JSON.stringify(params.observations) : undefined
  });

  if (!updatedData) {
    throw new BadRequestError('Data to be updated not found');
  }

  return {
    ...updatedData,
    observations: JSON.parse(updatedData.observations)
  }
}