import { TaskInput as Input, TaskOutput as Output } from '@folipro/communication/types/task';
import { repository } from '../providers/repository';

type Core = (params: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;

export const getManyPaginated: Core = async (params) => {
  const { data, total, limit, page } =  await repository.findManyPaginated(params);

  const mappedData = data.map(client => ({
    ...client,
    observations: JSON.parse(client.observations),
  }));

  return { data: mappedData, total, limit, page }
}