import { TaskInput as Input, TaskOutput as Output } from '@folipro/communication/types/task';
import { repository } from '../providers/repository';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async (params) => {
  const createdData = await repository.insert({
    ...params,
    observations: params.observations && params.observations.length
      ? JSON.stringify(params.observations)
      : JSON.stringify([])
  });

  return {
    ...createdData,
    observations: JSON.parse(createdData.observations)
  }
}