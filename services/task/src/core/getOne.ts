import { TaskInput as Input, TaskOutput as Output } from '@folipro/communication/types/task';
import { repository } from '../providers/repository';

type Core = (params: Input.GetOne) => Promise<Output.GetOne>;

export const getOne: Core = async (params) => {
  const foundData = await repository.findOne(params);
  if (!foundData) return undefined;
  return {
    ...foundData,
    observations: JSON.parse(foundData.observations),
  }
}