import {
  TaskServiceRouter as ServiceRouter,
  TaskInput as Input,
  TaskMethods as Methods
} from '@folipro/communication/types/task';
import { TaskModel as Model } from '@folipro/types/services/task';
import { routerHandler, Router, Url } from '@folipro/router';
import { Joi } from '@folipro/validator';
import { serviceName } from './utils/serviceInfo';
import * as core from './core';

const url = (url: Url) => `/${serviceName}${url}`;
const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

const whereJoi = Joi.object<Partial<Model.WithoutDates>>().keys({
  id: Joi.number().optional(),
  categoryId: Joi.number().optional().allow(null),
  orderId: Joi.number().optional(),
  clientId: Joi.number().optional(),
  dayToBeRemembered: Joi.string().optional(),
  isFinished: Joi.boolean().optional(),
});

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  },

  [Methods.Create]: {
    handler: core.create,
    schema: Joi.object<Input.Create>().keys({
      categoryId: Joi.number().optional(),
      orderId: Joi.number().required(),
      clientId: Joi.number().required(),
      dayToBeRemembered: Joi.string().optional(),
      isFinished: Joi.boolean().required(),
      observations: Joi.array().items(Joi.string().optional())
    })
  },

  [Methods.Update]: {
    handler: core.update,
    schema: Joi.object<Input.Update>().keys({
      id: Joi.number().required(),
      categoryId: Joi.number().optional().allow(null),
      orderId: Joi.number().optional(),
      clientId: Joi.number().optional(),
      dayToBeRemembered: Joi.string().optional(),
      isFinished: Joi.boolean().optional(),
      observations: Joi.array().items(Joi.string().optional())
    })
  },

  [Methods.GetOne]: {
    handler: core.getOne,
    schema: Joi.object<Input.GetOne>().keys({
      where: whereJoi.required()
    })
  },

  [Methods.GetMany]: {
    handler: core.getMany,
    schema: Joi.object<Input.GetMany>().keys({
      where: whereJoi.optional()
    })
  },

  [Methods.GetManyPaginated]: {
    handler: core.getManyPaginated,
    schema: Joi.object<Input.GetManyPaginated>().keys({
      limit: Joi.number().optional(),
      page: Joi.number().optional(),
      where: whereJoi.optional()
    })
  },

  [Methods.Delete]: {
    handler: core.softDelete,
    schema: Joi.object<Input.Delete>().keys({
      id: Joi.number().required()
    })
  },
}));

export const routes = router;