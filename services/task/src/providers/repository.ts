import knex from 'knex';
import { TaskModel as Model } from '@folipro/types/services/task';
import { OrderBy, PaginatedParams, Remove } from '@folipro/types/shared';
import { BadRequestError } from '@folipro/error';

type ModelComplete = Remove<Model.Complete, 'observations'> & { observations: string; }
type ModelWithoutDatesAndId = Remove<Model.WithoutDatesAndId, 'observations'> & { observations: string; }

const getTable = () => {
  try {
    const database = knex({
      client: 'pg',
      connection: process.env.DB_URL
    });

    return database<ModelComplete>('tasks');
  } catch (err) {
    throw new Error(`Error on opening conn with PostgreSQL. Error details: ${err}`);
  }
}

type FindParams = {
  where: Partial<Remove<ModelComplete, 'createdAt' | 'updatedAt' | 'deletedAt' | 'observations'>>;
}

export const repository = {
  insert: async (params: ModelWithoutDatesAndId) => {
    try {
      const data = await getTable().insert(params).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  update: async ({ id, ...params }: Partial<ModelWithoutDatesAndId> & Pick<ModelComplete, 'id'>) => {
    try {
      const data = await getTable().where('id', id).whereNull('deletedAt').update({ ...params, updatedAt: new Date() }).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  delete: async ({ id }: Pick<ModelComplete, 'id'>) => {
    try {
      const data = await getTable().where('id', id).whereNull('deletedAt').update({ deletedAt: new Date() }).returning('*');
      return data[0];
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findOne: async ({ where }: FindParams) => {
    try {
      let query = getTable();

      if (where) {
        for (const [key, value] of Object.entries(where)) {
          query = query.where(key, value);
        }
      }

      return await query.where('deletedAt', null).where('isFinished', false).first();
    } catch (err: any) {
      throw new BadRequestError(err.message);
    }
  },

  findMany: async (params: Partial<FindParams & OrderBy> | void) => {
    let query = getTable();

    if (params && params.where) {
      for (const [key, value] of Object.entries(params.where)) {
        query = query.where(key, value);
      }
    }

    const orderBy = params && params.orderBy ? params.orderBy : 'createdAt';
    const order = params && params.order ? params.order : 'asc';

    return await query.where('deletedAt', null).where('isFinished', false).orderBy(orderBy, order).select('*');
  },

  findManyByCategoryId: async ({ categoryId }: Pick<Model.Complete, 'categoryId'>) => {
    if (!categoryId) {
      return await getTable().whereNull('categoryId').where('isFinished', false).orderBy('createdAt', 'asc').select('*');
    } else {
      return await getTable().where('categoryId', categoryId).where('isFinished', false).orderBy('createdAt', 'asc').select('*');
    }
  },

  findManyPaginated: async (params: void | Partial<FindParams & OrderBy & PaginatedParams>) => {
    let query = getTable();

    const [count] = await query.count('id');

    if (params && params.where) {
      for (const [key, value] of Object.entries(params.where)) {
        query = query.where(key, value);
      }
    }

    const orderBy = params && params.orderBy ? params.orderBy : 'createdAt';
    const order = params && params.order ? params.order : 'asc';
    const limit = params && params.limit ? params.limit : 10;
    const page = params && params.page ? params.page : 1;
    const offset = Math.ceil((page - 1) * limit);

    const data = await query.where('deletedAt', null).where('isFinished', false).orderBy(orderBy, order).limit(limit).offset(offset).select('*');

    return { data, total: +count.CNT, limit, page }
  },
}