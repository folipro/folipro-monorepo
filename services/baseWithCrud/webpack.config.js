const path = require('path')
const webpack = require('webpack')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: './src/index.ts',
  output: {
    libraryTarget: 'commonjs',
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist')
  },
  target: 'node',
  resolve: {
    extensions: ['.ts', '.webpack.js', '.web.js', '.mjs', '.js', '.json'],
    plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.json' })]
  },
  module: {
    rules: [{
      test: /\.ts$/,
      use: {
        loader: 'ts-loader',
        options: { transpileOnly: true, configFile: 'tsconfig.json' }
      }
    }]
  },
  optimization: {
    nodeEnv: false
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_PG_FORCE_NATIVE': undefined
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /^pg-native$/
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /\.\/native/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /sqlite3/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /mariasql/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /mssql/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /mysql/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /oracle/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /strong-oracle/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /oracledb/,
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /pg-query-stream/,
    })
  ]
}
