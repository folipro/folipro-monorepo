import { Service, ServicesNames } from '@folipro/types/shared';

const service = ServicesNames.BaseWithCrud;

export const serviceName = service;
export const servicePort = Service[serviceName].port;