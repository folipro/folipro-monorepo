import {
  BaseWithCrudInput as Input,
  BaseWithCrudOutput as Output
} from '@folipro/communication/types/baseWithCrud';
import { repository } from '../providers/repository';

type Core = (params: Input.GetOne) => Promise<Output.GetOne>;

export const getOne: Core = async (params) => {
  return await repository.findOne(params);
}