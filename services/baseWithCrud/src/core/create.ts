import {
  BaseWithCrudInput as Input,
  BaseWithCrudOutput as Output
} from '@folipro/communication/types/baseWithCrud';
import { repository } from '../providers/repository';

type Core = (params: Input.Create) => Promise<Output.Create>;

export const create: Core = async (params) => {
  return await repository.insert(params);
}