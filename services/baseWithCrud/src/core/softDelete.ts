import {
  BaseWithCrudInput as Input,
  BaseWithCrudOutput as Output
} from '@folipro/communication/types/baseWithCrud';
import { repository } from '../providers/repository';

type Core = (params: Input.Delete) => Promise<Output.Delete>;

export const softDelete: Core = async (params) => {
  const deletedData = await repository.delete(params);
  if (deletedData) return true;
  else return false;
}