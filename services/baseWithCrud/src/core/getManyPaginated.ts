import {
  BaseWithCrudInput as Input,
  BaseWithCrudOutput as Output
} from '@folipro/communication/types/baseWithCrud';
import { repository } from '../providers/repository';

type Core = (params: Input.GetManyPaginated) => Promise<Output.GetManyPaginated>;

export const getManyPaginated: Core = async (params) => {
  const { data, total, limit, page } =  await repository.findManyPaginated(params);
  return { data, total, limit, page }
}