import {
  BaseWithCrudInput as Input,
  BaseWithCrudOutput as Output
} from '@folipro/communication/types/baseWithCrud';
import { repository } from '../providers/repository';

type Core = (params: Input.GetMany) => Promise<Output.GetMany>;

export const getMany: Core = async (params) => {
  return await repository.findMany(params);
}