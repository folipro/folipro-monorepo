import {
  BaseWithCrudInput as Input,
  BaseWithCrudOutput as Output
} from '@folipro/communication/types/baseWithCrud';
import { BadRequestError } from '@folipro/error';
import { repository } from '../providers/repository';

type Core = (params: Input.Update) => Promise<Output.Update>;

export const update: Core = async (params) => {
  const updatedData = await repository.update(params);
  if (!updatedData) {
    throw new BadRequestError('Data to be updated not found');
  }
  return updatedData;
}