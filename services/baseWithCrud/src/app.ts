import 'dotenv/config';
import 'express-async-errors';
import express from 'express';
import cors from 'cors';
import { errorHandler, verifyInternalApiKey, routeNotFound } from '@folipro/middlewares';
import { routes } from './routes';

const app = express();

app.use(cors());
app.use(express.json());
app.use(verifyInternalApiKey);
app.use(routes);
app.use(routeNotFound);
app.use(errorHandler);

export { app }