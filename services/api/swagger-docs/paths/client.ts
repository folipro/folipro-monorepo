import * as client from '../../src/controllers/client';
import { makePath, makeResponse } from '../utils';

export const clientPaths = {
  '/client': makePath('client', {
    get: {
      summary: 'Get all clients',
      description: 'Get all clients',
      response: makeResponse('Found all clients', {
        type: 'array',
        items: {
          $ref: '#/definitions/Client'
        }
      })
    },
  }),

  '/client/like/email/{email}': makePath('client', {
    get: {
      summary: 'Get all clients that e-mail is similar',
      description: 'Get all clients that e-mail is similar',
      request: {
        params: [
          { in: 'path', name: 'email', required: true, type: 'string' },
        ]
      },
      response: makeResponse('Found all clients that e-mail is similar', {
        type: 'array',
        items: {
          $ref: '#/definitions/Client'
        }
      })
    },
  }),

  '/client/like/cpfOrCnpj/{cpfOrCnpj}': makePath('client', {
    get: {
      summary: 'Get all clients that cpfOrCnpj is similar',
      description: 'Get all clients that cpfOrCnpj is similar',
      request: {
        params: [
          { in: 'path', name: 'cpfOrCnpj', required: true, type: 'string' },
        ]
      },
      response: makeResponse('Found all clients that cpfOrCnpj is similar', {
        type: 'array',
        items: {
          $ref: '#/definitions/Client'
        }
      })
    },
  }),

  '/client/like/name/{name}': makePath('client', {
    get: {
      summary: 'Get all clients that name is similar',
      description: 'Get all clients that name is similar',
      request: {
        params: [
          { in: 'path', name: 'name', required: true, type: 'string' },
        ]
      },
      response: makeResponse('Found all clients that name is similar', {
        type: 'array',
        items: {
          $ref: '#/definitions/Client'
        }
      })
    },
  }),

  '/client/{id}': makePath('client', {
    get: {
      summary: 'Get client by ID',
      description: 'Get client by ID',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ]
      },
      response: makeResponse('Found client', '#/definitions/Client')
    },
    put: {
      summary: 'Update client',
      description: 'Update client',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ],
        body: {
          schema: client.updateSchema,
          description: 'Update client body'
        }
      },
      response: makeResponse('Updated client', '#/definitions/Client')
    },
    delete: {
      summary: 'Delete client',
      description: 'Delete client',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ]
      },
      response: makeResponse('Deleted client', {
        type: 'boolean'
      })
    }
  }),
}