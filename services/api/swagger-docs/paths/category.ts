import * as category from '../../src/controllers/category';
import { makePath, makeResponse } from '../utils';

export const categoryPaths = {
  '/category': makePath('category', {
    get: {
      summary: 'Get all categories',
      description: 'Get all categories',
      response: makeResponse('Found all categories', {
        type: 'array',
        items: {
          $ref: '#/definitions/Category'
        }
      })
    },

    post: {
      summary: 'Create category',
      description: 'Create category',
      request: {
        body: {
          schema: category.createSchema,
          description: 'Create category body'
        }
      },
      response: makeResponse('Created category', '#/definitions/Category')
    }
  }),

  '/category/{id}': makePath('category', {
    put: {
      summary: 'Update category',
      description: 'Update category',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ],
        body: {
          schema: category.updateSchema,
          description: 'Update category body'
        }
      },
      response: makeResponse('Updated category', '#/definitions/Category')
    },
    delete: {
      summary: 'Delete category',
      description: 'Delete category',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ]
      },
      response: makeResponse('Deleted category', {
        type: 'boolean'
      })
    }
  }),
}