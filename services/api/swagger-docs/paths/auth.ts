import { loginSchema } from '../../src/controllers/auth';
import { makePath, makeResponse } from '../utils';

export const authPaths = {
  '/public/auth/login': makePath('auth', {
    post: {
      isPublic: true,
      summary: 'Login',
      description: 'Login',
      request: {
        body: {
          description: 'Login request body',
          schema: loginSchema
        }
      },
      response: makeResponse('Login is OK', {
        type: 'string'
      })
    }
  })
}