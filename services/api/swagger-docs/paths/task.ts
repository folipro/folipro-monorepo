import * as task from '../../src/controllers/task';
import { makePath, makeResponse } from '../utils';

export const taskPaths = {
  '/task/without-category/paginated/{limit}/{page}': makePath('task', {
    get: {
      summary: 'Get all tasks without category',
      description: 'Get all tasks without category',
      request: {
        params: [
          { in: 'path', name: 'limit', type: 'integer', required: true },
          { in: 'path', name: 'page', type: 'integer', required: true },
        ]
      },
      response: makeResponse('Found all tasks without category', {
        type: 'array',
        items: {
          properties: {
            task: {
              $ref: '#/definitions/Task'
            },
            client: {
              $ref: '#/definitions/Client'
            },
            order: {
              $ref: '#/definitions/Order'
            },
          }
        }
      })
    },
  }),

  '/task/{id}': makePath('task', {
    get: {
      summary: 'Get task by ID',
      description: 'Get task by ID',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ]
      },
      response: makeResponse('Found task', '#/definitions/Task')
    },
    put: {
      summary: 'Update task',
      description: 'Update task',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ],
        body: {
          schema: task.updateTaskSchema,
          description: 'Update task body'
        }
      },
      response: makeResponse('Updated task', '#/definitions/Task')
    },
    delete: {
      summary: 'Delete task',
      description: 'Delete task',
      request: {
        params: [
          { in: 'path', name: 'id', required: true, type: 'integer' },
        ]
      },
      response: makeResponse('Deleted task', {
        type: 'boolean'
      })
    }
  }),
}