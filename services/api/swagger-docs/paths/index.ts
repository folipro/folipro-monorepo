import { pingPaths } from './ping';
import { authPaths } from './auth';
import { categoryPaths } from './category';
import { clientPaths } from './client';
import { taskPaths } from './task';
import { orderPaths } from './order';

export const paths = {
  ...pingPaths,
  ...authPaths,
  ...categoryPaths,
  ...clientPaths,
  ...orderPaths,
  ...taskPaths,
}