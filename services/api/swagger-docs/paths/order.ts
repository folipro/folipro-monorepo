// import * as order from '../../src/controllers/order';
import { makePath, makeResponse } from '../utils';

export const orderPaths = {
  '/order/by-client/paginated/{clientId}/{limit}/{page}': makePath('order', {
    get: {
      summary: 'Get all orders of a client',
      description: 'Get all orders of a client',
      request: {
        params: [
          { in: 'path', name: 'clientId', type: 'integer', required: true },
          { in: 'path', name: 'limit', type: 'integer', required: true },
          { in: 'path', name: 'page', type: 'integer', required: true },
        ]
      },
      response: makeResponse('Found all orders of a client', {
        type: 'array',
        items: {
          properties: {
            $ref: '#/definitions/Order'
          }
        }
      })
    },
  }),
}