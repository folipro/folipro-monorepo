import { definitions } from './definitions';
import { paths } from './paths';

export const swaggerJson = {
  swagger: '2.0',
  info: {
    title: 'Folipro API',
    description: '',
    version: '1.0.0',
  },
  produces: [
    'application/json'
  ],
  host: 'ggl6m6c5e3.execute-api.us-east-1.amazonaws.com',
  basePath: '/prod/api',
  schemes: ['https'],
  definitions,
  paths,
  tags: [
    { name: 'health-check', description: 'Ping route' },
    { name: 'auth', description: 'Everything related to auth' },
    { name: 'category', description: 'Everything related to category' },
    { name: 'client', description: 'Everything related to client' },
    { name: 'order', description: 'Everything related to order' },
    { name: 'task', description: 'Everything related to task' },
  ],
  securityDefinitions: {
    auth: {
      type: 'basic'
    }
  }
}