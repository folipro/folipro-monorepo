import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';
import { CategoryModel } from '@folipro/types/services/category';

export const Category = joiToSwagger(Joi.object<CategoryModel.WithoutDates>().keys({
  id: Joi.number().required()
    .description('category ID')
    .example(1),
  name: Joi.string().required()
    .description('category name')
    .example('D+30'),
  color: Joi.string().required()
    .description('category color')
    .example('E9E9E9 (HEX color without #)'),
  daysToRememberValue: Joi.number().required()
    .description('days value')
    .example(30),
})).swagger;