import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';
import { OrderHelpers, OrderModel } from '@folipro/types/services/order';

export const Order = joiToSwagger(Joi.object<OrderModel.WithoutDates>().keys({
  id: Joi.number().required()
    .description('client ID')
    .example(1),
  clientId: Joi.number().required()
    .description('client ID')
    .example(1),
  paymentTransactionDate: Joi.string().required()
    .description('payment transaction date')
    .example('---'),
  totalAmount: Joi.number().required()
    .description('totalAmount')
    .example(100),
  totalAmountWithoutShippingCost: Joi.number().required()
    .description('totalAmountWithoutShippingCost')
    .example(100),
  products: Joi.array().items(Joi.object<OrderHelpers.OrderProduct>().keys({
    amount: Joi.number().required()
      .description('amount')
      .example(100),
    quantity: Joi.number().required()
      .description('quantity')
      .example(100),
    title: Joi.string().required()
      .description('title')
      .example('Titulo do produto'),
  })).required(),
})).swagger;