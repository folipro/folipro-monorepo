import { Category } from './Category';
import { Client } from './Client';
import { BadRequestError } from './BadRequestError';
import { SchemaValidationError } from './SchemaValidationError';
import { Task } from './Task';
import { Order } from './Order';

export const definitions = {
  Category,
  Client,
  BadRequestError,
  SchemaValidationError,
  Order,
  Task,
}