import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';
import { ClientModel } from '@folipro/types/services/client';

export const Client = joiToSwagger(Joi.object<ClientModel.WithoutDates>().keys({
  id: Joi.number().required()
    .description('client ID')
    .example(1),
  name: Joi.string().required()
    .description('client name')
    .example('Joao da Silva'),
  nameSearchable: Joi.string().required()
    .description('client name searchable')
    .example('joao-da-silva'),
  birthday: Joi.string().required()
    .description('birthday')
    .example('dd/mm/yyyy'),
  cpfOrCnpj: Joi.string().required()
    .description('cpfOrCnpj')
    .example('CPF or CNPJ'),
  email: Joi.string().email().required()
    .description('client email')
    .example('joao@gmail.com')
})).swagger;