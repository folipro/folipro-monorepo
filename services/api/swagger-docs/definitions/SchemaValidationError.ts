export const SchemaValidationError = {
  description: 'Schema Validation',
  type: 'object',
  properties: {
    errors: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  },
  example: {
    errors: [
      'field is required'
    ]
  }
}