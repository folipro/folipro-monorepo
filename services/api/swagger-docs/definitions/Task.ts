import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';
import { TaskModel } from '@folipro/types/services/task';

export const Task = joiToSwagger(Joi.object<TaskModel.WithoutDates>().keys({
  id: Joi.number().required()
    .description('client ID')
    .example(1),
  clientId: Joi.number().required()
    .description('client ID')
    .example(1),
  orderId: Joi.number().required()
    .description('order ID')
    .example(1),
  categoryId: Joi.number().optional()
    .description('category ID')
    .example(1),
  isFinished: Joi.boolean().required()
    .description('flag to indicate if the task is completely finished')
    .example(true),
  observations: Joi.array().items(Joi.string().required()).required()
    .description('array of observations')
    .example(['obs1', 'obs2']),
  dayToBeRemembered: Joi.string().required()
    .description('date tha it will be remembered (format yyyy-mm-dd)')
    .example('2022-05-25'),
})).swagger;