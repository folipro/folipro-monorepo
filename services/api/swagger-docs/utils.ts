import Joi from 'joi';
import joiToSwagger from 'joi-to-swagger';

type Params = {
  [method: string]: {
    summary: string;
    description: string;
    isPublic?: boolean;
    request?: {
      body?: {
        schema: Joi.ObjectSchema;
        description?: string;
      }
      params?: Array<{
        in: 'path';
        name: string;
        description?: string;
        required: boolean;
        type: string;
      }>;
    }
    response: object;
  }
};

export const makePath = (tag: 'health-check' | 'auth' | 'category' | 'client' | 'task' | 'order', params: Params) => {
  const finalObject = {}

  for (const [method, value] of Object.entries(params)) {
    const parameters = [];

    if (value.request && value.request.body) {
      parameters.push({
        in: 'body',
        name: 'body',
        description: value.request.body.description,
        schema: joiToSwagger(value.request.body.schema).swagger
      });
    }

    if (value.request && value.request.params && value.request.params.length) {
      for (const param of value.request.params) {
        parameters.push({
          in: param.in,
          name: param.name,
          description: param.description,
          required: param.required,
          type: param.type
        });
      }
    }

    Object.assign(finalObject, {
      [method]: {
        summary: value.summary,
        description: value.description,
        tags: [tag],
        parameters,
        security: value.isPublic ? [] : [{ auth: [] }],
        responses: {
          200: value.response,
          400: {
            description: "Bad Request Error",
            schema: {
              "$ref": "#/definitions/BadRequestError"
            }
          },
          422: {
            description: "Schema Validation Error",
            schema: {
              "$ref": "#/definitions/SchemaValidationError"
            }
          }
        }
      }
    });
  }

  return finalObject
}

type ObjectSchema = {
  type: string;
  required?: string[];
  items?: object | string;
  properties?: object;
}

export const makeResponse = (description: string, objSchema: ObjectSchema | string) => {
  let schema = {}

  if (typeof objSchema === 'string') schema = { $ref: objSchema }
  else schema = objSchema;

  return { description, schema }
};