import { sign, verify } from 'jsonwebtoken';

const { JWT_SECRET } = process.env;
if (!JWT_SECRET) {
  throw new Error('process.env.JWT_SECRET is missing');
}

type EmailAndPassword = { email: string; password: string; }

export const generateJwt = ({ email, password }: EmailAndPassword) => {
  return sign({ email, password }, JWT_SECRET);
}

export const decodeJwt = (jwtToken: string) => {
  return verify(jwtToken, JWT_SECRET) as EmailAndPassword;
}