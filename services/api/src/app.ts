import 'dotenv/config';
import 'express-async-errors';
import cors from 'cors';
import express from 'express';
import { errorHandler, routeNotFound } from '@folipro/middlewares';
import { jwtDecoder } from './middlewares';
import { routes } from './routes';

const app = express();

app.use(cors());
app.use(express.json());
app.use(jwtDecoder);
app.use('/api', routes);
app.use(routeNotFound);
app.use(errorHandler);

export { app }