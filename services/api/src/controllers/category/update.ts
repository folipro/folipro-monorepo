import { Joi } from '@folipro/validator';
import { categoryService } from '@folipro/communication/services/category';
import { CategoryInput } from '@folipro/communication/types/category';
import { CategoryModel } from '@folipro/types/services/category';
import { Remove } from '@folipro/types/shared';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = Remove<CategoryInput.Update, 'id'>;
type Params = Pick<CategoryInput.Update, 'id'>;
type Query = {};
type Return = CategoryModel.Complete;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const updateController: Controller = async (request) => {
  validateSchema<Body>(request.body, updateSchema);

  const { id } = request.params;
  const { color, name, daysToRememberValue } = request.body;

  return await categoryService.update({ color, name, daysToRememberValue, id });
}

export const updateSchema = Joi.object<Body>().keys({
  name: Joi.string().optional(),
  color: Joi.string().optional(),
  daysToRememberValue: Joi.number().optional(),
});