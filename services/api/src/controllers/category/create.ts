import { Joi } from '@folipro/validator';
import { categoryService } from '@folipro/communication/services/category';
import { CategoryInput } from '@folipro/communication/types/category';
import { CategoryModel } from '@folipro/types/services/category';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = CategoryInput.Create;
type Params = {};
type Query = {};
type Return = CategoryModel.Complete;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const createController: Controller = async (request) => {
  validateSchema<Body>(request.body, createSchema);

  const { color, name, daysToRememberValue } = request.body;

  return await categoryService.create({ color, name, daysToRememberValue });
}

export const createSchema = Joi.object<Body>().keys({
  name: Joi.string().required(),
  color: Joi.string().required(),
  daysToRememberValue: Joi.number().required(),
});