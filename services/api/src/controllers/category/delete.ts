import { categoryService } from '@folipro/communication/services/category';
import { CategoryInput } from '@folipro/communication/types/category';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = Pick<CategoryInput.Update, 'id'>;
type Query = {};
type Return = boolean;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const deleteController: Controller = async (request) => {
  const { id } = request.params;
  return await categoryService.delete({ id });
}