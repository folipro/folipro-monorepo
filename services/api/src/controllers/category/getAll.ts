import { categoryService } from '@folipro/communication/services/category';
import { CategoryModel } from '@folipro/types/services/category';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = {};
type Query = {};
type Return = CategoryModel.Complete[];

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllController: Controller = async () => {
  return await categoryService.getMany();
}