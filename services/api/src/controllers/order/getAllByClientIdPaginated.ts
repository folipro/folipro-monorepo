import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';
import { orderService } from '@folipro/communication/services/order';
import { OrderModel } from '@folipro/types/services/order';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = PaginatedParams & { clientId: number; };
type Query = {};
type Return = PaginatedReturn & { data: OrderModel.Complete[]; }

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllByClientIdPaginatedController: Controller = async (request) => {
  const { limit, page, clientId } = request.params;

  return await orderService.getManyPaginated({
    limit,
    page,
    where: { clientId }
  });
}