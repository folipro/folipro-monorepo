import { taskService } from '@folipro/communication/services/task';
import { TaskInput } from '@folipro/communication/types/task';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = Pick<TaskInput.Delete, 'id'>;
type Query = {};
type Return = boolean;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const deleteController: Controller = async (request) => {
  const { id } = request.params;
  return await taskService.delete({ id });
}