import { addDays } from 'date-fns';
import { categoryService } from '@folipro/communication/services/category';
import { taskService } from '@folipro/communication/services/task';
import { TaskInput } from '@folipro/communication/types/task';
import { TaskModel } from '@folipro/types/services/task';
import { BadRequestError } from '@folipro/error';
import { Joi } from '@folipro/validator';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = Pick<TaskInput.Update, 'observations' | 'categoryId' | 'isFinished'>;
type Params = Pick<TaskInput.Update, 'id'>;
type Query = {};
type Return = TaskModel.Complete;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const updateTaskController: Controller = async (request) => {
  const { body, params } = request;

  validateSchema<Body>(body, updateTaskSchema);

  const task = await taskService.getOne({ where: { id: params.id } });

  if (!task) {
    throw new BadRequestError('Task not found');
  }

  if (body.isFinished) {
    return await taskService.update({
      id: params.id,
      categoryId: null,
      observations: body.observations,
      isFinished: true
    });
  }

  let dayToBeRemembered: string | undefined;

  if (body.categoryId) {
    const category = await categoryService.getOne({ where: { id: body.categoryId } });

    if (!category) {
      throw new BadRequestError('Category not found');
    }

    const taskDayToBeRemembered = task.dayToBeRemembered && task.dayToBeRemembered.includes('T')
      ? task.dayToBeRemembered.split('T')[0]
      : task.dayToBeRemembered;

    dayToBeRemembered = addDays(
      new Date(taskDayToBeRemembered || ''),
      category.daysToRememberValue
    ).toISOString().split('T')[0];
  }

  return await taskService.update({
    id: params.id,
    categoryId: body.categoryId,
    observations: body.observations,
    dayToBeRemembered
  });
}

export const updateTaskSchema = Joi.object<Body>().keys({
  categoryId: Joi.number().optional(),
  observations: Joi.array().items(Joi.string().optional()).optional(),
  isFinished: Joi.boolean().optional()
});