import { taskService } from '@folipro/communication/services/task';
import { TaskModel } from '@folipro/types/services/task';
import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateDateString } from '../../utils/validateDate';

type Body = {};
type Params = PaginatedParams & Pick<TaskModel.Complete, 'categoryId'> & { dateReference: string; };
type Query = {};
type Return = PaginatedReturn & { data: TaskModel.Complete[]; }

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllByCategoryIdPaginatedController: Controller = async (request) => {
  const { categoryId, dateReference, limit, page } = request.params;

  const tasksPaginated = await taskService.getManyPaginated({
    limit,
    page,
    where: { categoryId }
  });

  validateDateString(dateReference);
  return tasksPaginated;

  // return tasksPaginated.data.filter(task => {
  //   if (!task.dayToBeRemembered) return false;

  //   const diffInDays = differenceInDays(
  //     new Date(task.dayToBeRemembered),
  //     new Date(dateReference)
  //   );
  //   return diffInDays > 0;
  // });
}