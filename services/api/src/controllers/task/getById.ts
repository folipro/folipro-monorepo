import { TaskModel } from '@folipro/types/services/task';
import { taskService } from '@folipro/communication/services/task';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = Pick<TaskModel.Complete, 'id'>;
type Query = {};
type Return = TaskModel.Complete | undefined;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getByIdController: Controller = async (request) => {
  const { id } = request.params;
  return await taskService.getOne({ where: { id } });
}