import { PaginatedParams, PaginatedReturn } from '@folipro/types/shared';
import { clientService } from '@folipro/communication/services/client';
import { orderService } from '@folipro/communication/services/order';
import { taskService } from '@folipro/communication/services/task';
import { ClientModel } from '@folipro/types/services/client';
import { OrderModel } from '@folipro/types/services/order';
import { TaskModel } from '@folipro/types/services/task';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = PaginatedParams;
type Query = {};
type Return = PaginatedReturn & { data: Data; }

type Data = Array<{
  task: TaskModel.Complete;
  order: OrderModel.Complete;
  client: ClientModel.Complete;
}>;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllWithoutCategoryIdPaginatedController: Controller = async (request) => {
  const { limit, page } = request.params;

  const tasksPaginated = await taskService.getManyPaginated({
    limit,
    page,
    where: { categoryId: null }
  });

  const finalArray: Data = [];

  for (const task of tasksPaginated.data) {
    const [order, client] = await Promise.all([
      orderService.getOne({ where: { id: task.orderId } }),
      clientService.getOne({ where: { id: task.clientId } })
    ]);

    if (order && client) finalArray.push({ task, order, client });
  }

  return {
    ...tasksPaginated,
    data: finalArray
  };
}