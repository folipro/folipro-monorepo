import { BadRequestError } from '@folipro/error';
import { Joi } from '@folipro/validator';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';
import { generateJwt } from '../../providers/jwt';

type Body = { email: string; password: string; }
type Params = {};
type Query = {};
type Return = string;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

const { ADMIN_EMAIL, ADMIN_PASSWORD } = process.env;

if (!ADMIN_EMAIL || !ADMIN_PASSWORD) {
  throw new Error('You must provide admin auth credentials.');
}

export const loginController: Controller = async (request) => {
  validateSchema<Body>(request.body, loginSchema);

  const { email, password } = request.body;

  if (email !== ADMIN_EMAIL || password !== ADMIN_PASSWORD) {
    throw new BadRequestError('Invalid login credentials');
  }

  return generateJwt({ email, password });
}

export const loginSchema = Joi.object<Body>().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required()
});