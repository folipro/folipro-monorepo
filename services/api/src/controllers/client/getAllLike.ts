import { clientService } from '@folipro/communication/services/client';
import { BadRequestError } from '@folipro/error';
import { ClientModel } from '@folipro/types/services/client';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = Partial<Pick<ClientModel.Complete, 'email' | 'cpfOrCnpj'>> & { name?: string; };
type Query = {};
type Return = ClientModel.Complete[];

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllLikeController: Controller = async (request) => {
  const { email, cpfOrCnpj, name } = request.params;
  
  if (email) return await clientService.getManyLike({ whereLike: { email } });
  if (cpfOrCnpj) return await clientService.getManyLike({ whereLike: { cpfOrCnpj } });
  if (name) return await clientService.getManyLike({ whereLike: { nameSearchable: name } });

  throw new BadRequestError(
    'You must provide one of "email", "cpfOrCnpj" or "name".'
  );
}