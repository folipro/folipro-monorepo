import { ClientModel } from '@folipro/types/services/client';
import { clientService } from '@folipro/communication/services/client';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = Pick<ClientModel.Complete, 'id'>;
type Query = {};
type Return = ClientModel.Complete | undefined;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getByIdController: Controller = async (request) => {
  const { id } = request.params;
  return await clientService.getOne({ where: { id } });
}