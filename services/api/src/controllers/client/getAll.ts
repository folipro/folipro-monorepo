import { clientService } from '@folipro/communication/services/client';
import { ClientModel } from '@folipro/types/services/client';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = {};
type Query = {};
type Return = ClientModel.Complete[];

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const getAllController: Controller = async () => {
  return await clientService.getMany();
}