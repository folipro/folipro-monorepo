import { clientService } from '@folipro/communication/services/client';
import { ClientInput } from '@folipro/communication/types/client';
import { ControllerWrapper } from '../../utils/controllerWrapper';

type Body = {};
type Params = Pick<ClientInput.Update, 'id'>;
type Query = {};
type Return = boolean;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const deleteController: Controller = async (request) => {
  const { id } = request.params;
  return await clientService.delete({ id });
}