import { Joi } from '@folipro/validator';
import { clientService } from '@folipro/communication/services/client';
import { ClientInput } from '@folipro/communication/types/client';
import { ClientModel } from '@folipro/types/services/client';
import { Remove } from '@folipro/types/shared';
import { ControllerWrapper } from '../../utils/controllerWrapper';
import { validateSchema } from '../../utils/validateSchema';

type Body = Remove<ClientInput.Update, 'id'>;
type Params = Pick<ClientInput.Update, 'id'>;
type Query = {};
type Return = ClientModel.Complete;

type Controller = ControllerWrapper<Body, Params, Query, Return>;

export const updateController: Controller = async (request) => {
  validateSchema<Body>(request.body, updateSchema);

  const { id } = request.params;

  return await clientService.update({ id, ...request.body });
}

export const updateSchema = Joi.object<Body>().keys({
  email: Joi.string().email().optional(),
  name: Joi.string().optional(),
  cpfOrCnpj: Joi.string().optional(),
  birthday: Joi.string().optional(),
  address: Joi.any().optional(),
  phoneNumber: Joi.any().optional()
});