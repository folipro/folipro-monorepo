import { BadRequestError } from '@folipro/error';

export const validateDateString = (dateString: string) => {
  const [year, month, day] = dateString.split('-');

  if (!year || !month || !day) {
    throw new BadRequestError('Date must be in format yyyy-mm-dd')
  }

  if (String(year).length !== 4) {
    throw new BadRequestError('Year must have 4 digits');
  }

  if (String(month).length !== 2) {
    throw new BadRequestError('Month must have 2 digits');
  }

  if (String(day).length !== 2) {
    throw new BadRequestError('Day must have 2 digits');
  }

  if (Number(day) > 31) {
    throw new BadRequestError('Max day is 31');
  }

  if (Number(month) > 12) {
    throw new BadRequestError('Month must be between 1 (January) and 12 (December)');
  }
}