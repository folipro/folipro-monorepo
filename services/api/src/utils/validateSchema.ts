import Joi from 'joi';
import { InvalidParamError } from '@folipro/error';

export const validateSchema = <T>(body: T, schema: Joi.ObjectSchema<T>) => {
  const errorsArray = [];

  const { error } = schema.validate(body, { abortEarly: false });

  if (error) {
    for (const err of error.details) {
      errorsArray.push(err.message);
    }
  }

  if (errorsArray.length) {
    throw new InvalidParamError(errorsArray);
  }
}