// import { UserModel } from '@folipro/types/services/user';
import { Request, Response } from 'express';

// declare global {
//   namespace Express {
//     interface Request {
//       user: UserModel.Complete;
//     }
//   }
// }

export type ControllerParams<Body, UrlParam, UrlQuery> = {
  body: Body;
  params: UrlParam;
  query: UrlQuery;
  // user: UserModel.Complete;
}

export type ControllerWrapper<Body = {}, Param = {}, Query = {}, Response = any> = ({ body, params, query }: ControllerParams<Body, Param, Query>) => Promise<Response>;

export const controllerWrapper = (functionController: ControllerWrapper<any, any, any>) => {
  return async (request: Request, response: Response) => {
    try {
      const result = await functionController({
        body: request.body,
        params: request.params,
        query: request.query,
        // user: request.user
      });
  
      return response.json(result);
    } catch (err: any) {
      if (err.isAppError) throw err;

      let errors = ['Internal server error'];
      if (err.message) errors = err.message;
      
      return response.status(500).json({ errors });
    }
  }
}