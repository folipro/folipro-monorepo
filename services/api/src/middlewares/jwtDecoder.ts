import { Request, Response, NextFunction, response } from 'express';
import { UnauthorizedError } from '@folipro/error';
import { decodeJwt } from '../providers/jwt';

export const jwtDecoder = (request: Request, _: Response, next: NextFunction) => {
  if (request.originalUrl.includes('/public')) {
    return next();
  }

  const { authorization } = request.headers;
  if (!authorization) {
    throw new UnauthorizedError('There is no JWT token');
  }

  const jwtToken = authorization.replace('Bearer', '').trim();

  const verifiedToken = decodeJwt(jwtToken);
  if (!verifiedToken) {
    throw new UnauthorizedError('Invalid Token');
  }

  if (verifiedToken.email !== process.env.ADMIN_EMAIL) {
    throw new UnauthorizedError('Invalid user credentials');
  }

  if (verifiedToken.password !== process.env.ADMIN_PASSWORD) {
    throw new UnauthorizedError('Invalid user credentials');
  }

  return next();
}