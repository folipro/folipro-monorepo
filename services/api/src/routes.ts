import swaggerUi from 'swagger-ui-express';
import { Router } from 'express';
import { swaggerJson } from '../swagger-docs';
import { controllerWrapper as wrapper } from './utils/controllerWrapper';
import { serviceName } from './utils/serviceInfo';
import * as auth from './controllers/auth';
import * as category from './controllers/category';
import * as client from './controllers/client';
import * as task from './controllers/task';
import * as order from './controllers/order';

const router = Router();

if (process.env.APP_MODE === 'local') {
  router.use('/public/api-docs', swaggerUi.serve);
  router.get('/public/api-docs', swaggerUi.setup(swaggerJson));
} else {
  router.get('/public/api-docs', (_, response) => response.json(swaggerJson));
}

router.get('/public/ping', (_, response) => response.json(`PING on service ${serviceName}`));

router.post('/public/auth/login', wrapper(auth.loginController));

router.post('/category', wrapper(category.createController));
router.get('/category', wrapper(category.getAllController));
router.put('/category/:id', wrapper(category.updateController));
router.delete('/category/:id', wrapper(category.deleteController));

const getAllLikeController = wrapper(client.getAllLikeController);
router.get('/client', wrapper(client.getAllController));
router.get('/client/like/email/:email', getAllLikeController);
router.get('/client/like/cpfOrCnpj/:cpfOrCnpj', getAllLikeController);
router.get('/client/like/name/:name', getAllLikeController);
router.get('/client/:id', wrapper(client.getByIdController));
router.put('/client/:id', wrapper(client.updateController));
router.delete('/client/:id', wrapper(client.deleteController));

router.get('/task/without-category/paginated/:limit/:page', wrapper(task.getAllWithoutCategoryIdPaginatedController));
// router.get('/task/by-category/paginated/:categoryId/:referenceDate/:limit/:page', wrapper(task.getAllByCategoryIdPaginatedController));
router.get('/task/:id', wrapper(task.getByIdController));
router.put('/task/:id', wrapper(task.updateTaskController));
router.delete('/task/:id', wrapper(task.deleteController));

router.get('/order/by-client/paginated/:clientId/:limit/:page', wrapper(order.getAllByClientIdPaginatedController));

export const routes = router;