import { serviceName, servicePort } from './utils/serviceInfo';
import { app } from './app';

app.listen(servicePort, () => console.log(
  `Listening service ${serviceName.toUpperCase()} | http://localhost:${servicePort}/${serviceName}/public/ping`
));