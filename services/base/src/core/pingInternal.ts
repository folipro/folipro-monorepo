import { UserInput as Input, UserOutput as Output } from '@folipro/communication/types/user';
import { serviceName } from '../utils/serviceInfo';

type Core = (input: Input.PingInternal) => Promise<Output.PingInternal>;

export const pingInternal: Core = async () => `PING on service ${serviceName} [INTERNAL]`;