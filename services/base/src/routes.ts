import {
  BaseServiceRouter as ServiceRouter,
  BaseInput as Input,
  BaseMethods as Methods
} from '@folipro/communication/types/base';
import { routerHandler, Router } from '@folipro/router';
import { Joi } from '@folipro/validator';
import { serviceName } from './utils/serviceInfo';
import { url } from './utils/url';
import * as core from './core';

const router = Router();

router.get(url('/public/ping'), (_, response) => response.json(`PING on service ${serviceName}`));

router.post(url('/internal'), routerHandler<ServiceRouter>({
  [Methods.PingInternal]: {
    handler: core.pingInternal,
    schema: Joi.object<Input.PingInternal>().keys()
  }
}));

export const routes = router;