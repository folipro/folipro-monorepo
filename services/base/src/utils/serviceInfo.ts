import { Service, ServicesNames } from '@folipro/types/shared';

const service = ServicesNames.Base;

export const serviceName = service;
export const servicePort = Service[serviceName].port;