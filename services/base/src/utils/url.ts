import { Url } from '@folipro/router';
import { serviceName } from './serviceInfo';

export const url = (url: Url) => `/${serviceName}${url}`;